<?php

namespace PiotrOsinski\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Media
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="PiotrOsinski\Bundle\Entity\MediaRepository")
 * @ORM\HasLifecycleCallbacks
 *
 */
class Media {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="mime_type", type="string", length=50)
     * @ Assert\NotBlank
     */
    private $mimeType;

    /**
     * @ORM\Column(type="string", length=6, nullable=false)
     */
    private $ext;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @Assert\Image(
     *     minWidth = 200,
     *     maxWidth = 2000,
     *     minHeight = 200,
     *     maxHeight = 2000
     * )
     * @Assert\File(maxSize="2M")
     */
    private $file;

    /**
     * @ORM\ManyToOne(targetEntity="Work", inversedBy="media", cascade={"remove"})
     */
    private $work;
    
    public function __toString()
    {
        return $this->getName();
    }

    public function getAbsolutePath() {
        return null === $this->ext ? null : $this->getUploadRootDir() . '/' . $this->id . '.' . $this->ext;
    }

    public function getWebPath() {
        return null === $this->ext ? null : $this->getUploadDir() . '/' . $this->id . '.' . $this->ext;
    }
    
    public function getAbsoluteWebPath() {
        return '/'.$this->getWebPath();
    }
    
    public function getAbsoluteWebPathWithTag() {
        return '<img src ="/'.$this->getWebPath().'"/>';
    }

    protected function getUploadRootDir() {
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir() {
        return 'uploads/media/' . $this->getDirName();
    }

    public function getDirName($id = null) {
        $id_md5 = $id ? md5($id) : md5($this->id);
        $dir_1 = substr($id_md5, 0, 3);

//        $dir_2 = substr($id_md5, 3, 3);
//        $dir = $dir_1.'/'.$dir_2;

        $dir = $dir_1;
        return $dir;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload() {
        if (null !== $this->file) {
            $this->ext = $this->file->guessExtension();
            $this->setMimetype($this->file->getMimeType());
            $this->setName($this->file->getClientOriginalName());
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload() {
        if (null === $this->file) {
            return;
        }

        try {
            $this->file->move(
                    $this->getUploadRootDir(), $this->id . '.' . $this->file->guessExtension()
            );
        } catch (\Exception $e) {
            throw new \RuntimeException(__FILE__ . ": {$e}:" . __LINE__);
        }

        unset($this->file);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set mimeType
     *
     * @param string $mimeType
     * @return Media
     */
    public function setMimeType($mimeType) {
        $this->mimeType = $mimeType;

        return $this;
    }

    /**
     * Get mimeType
     *
     * @return string 
     */
    public function getMimeType() {
        return $this->mimeType;
    }

    /**
     * Set ext
     *
     * @param string $ext
     * @return Media
     */
    public function setExt($ext) {
        $this->ext = $ext;

        return $this;
    }

    /**
     * Get ext
     *
     * @return string 
     */
    public function getExt() {
        return $this->ext;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Media
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set work
     *
     * @param \PiotrOsinski\Bundle\Entity\Work $work
     * @return Media
     */
    public function setWork(\PiotrOsinski\Bundle\Entity\Work $work = null) {
        $this->work = $work;

        return $this;
    }

    /**
     * Get work
     *
     * @return \PiotrOsinski\Bundle\Entity\Work 
     */
    public function getWork() {
        return $this->work;
    }

    public function getFile() {
        return $this->file;
    }

    public function setFile(\Symfony\Component\HttpFoundation\File\UploadedFile $file) {
        $this->file = $file;
        return $this;
    }

}
<?php

namespace PiotrOsinski\Bundle\Entity\Blog;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="PiotrOsinski\Bundle\Entity\Blog\PostRepository")
 * @UniqueEntity("slug")
 * @ORM\HasLifecycleCallbacks()
 */
class Post
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(name="content", type="text")
     */
    private $content;
    
    /**
     * @Assert\NotBlank()
     * @ORM\Column(name="excerpt", type="text")
     */
    private $excerpt;
    
    /**
     * @ORM\ManyToMany(targetEntity="Category", inversedBy="posts")
     * @ORM\JoinTable(name="post_category")
     **/
    private $categories;
    
    /**
     * @ORM\Column(name="type", type="integer", options={"comment"="
     *                  0-draft, 
     *                  1-published, 
     *                  2-not published(not accesible), 
     *                  3-accesible only by link"})
     */
    private $type;
    public static $typesList = array(0=>'Szkic',1=>'Opublikowany',2=>'Ukryty',3=>'Dostępny przez link');
    
    /**
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;
    
    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;
    
    /**
     * @ORM\Column(name="slug", type="string", length=255, unique=true, nullable=false)
     */
    private $slug;
    
    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function prePersist()
    {
        $string = $this->getTitle();
        $slug = \PiotrOsinski\Bundle\Service\Helper\Slugizer::get($string);
        $this->setSlug($slug);
    }
    
    public static function generateCacheKey($id = null, $slug = null){
        return 'post:'.$id.':'.substr(md5($slug), 0, 10);
    }
    
    public function getCacheKey(){
        return self::generateCacheKey($this->getId(), $this->getSlug());
    }
    
    /**
     * Get id
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Post
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Post
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
        $this->createdAt = new \DateTime;
        $this->updatedAt = new \DateTime;
    }
    
    /**
     * @ORM\PreUpdate
     */
    public function preUpdate(){
        $this->updatedAt = new \DateTime;
    }
    
    
    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategories()
    {
        return $this->categories;
    }
    
    public function getCategoriesString(){
        $str = array();
        foreach($this->getCategories() as $one){
            $str[] = $one->getName();
        }

        return implode(', ', $str);
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Post
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType($translate = false)
    {
        if($translate)
            return self::$typesList[$this->type];
        else
            return $this->type;
    }
    
    public static function getTypesList(){
        return self::$typesList;
    }

    /**
     * Add categories
     *
     * @param \PiotrOsinski\Bundle\Entity\Blog\Post $categories
     * @return Post
     */
    public function addCategorie(\PiotrOsinski\Bundle\Entity\Blog\Category $categories)
    {
        $this->categories[] = $categories;
    
        return $this;
    }

    /**
     * Remove categories
     *
     * @param \PiotrOsinski\Bundle\Entity\Blog\Post $categories
     */
    public function removeCategorie(\PiotrOsinski\Bundle\Entity\Blog\Category $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Post
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Post
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    
    /**
     * Set slug
     *
     * @param string $slug
     * @return Post
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    
        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set excerpt
     *
     * @param string $excerpt
     * @return Post
     */
    public function setExcerpt($excerpt)
    {
        $this->excerpt = $excerpt;
    
        return $this;
    }

    /**
     * Get excerpt
     *
     * @return string 
     */
    public function getExcerpt()
    {
        return $this->excerpt;
    }
}
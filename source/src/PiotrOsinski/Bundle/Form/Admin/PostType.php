<?php

namespace PiotrOsinski\Bundle\Form\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use PiotrOsinski\Bundle\Form\Extended\PostTypeChoiceList;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('excerpt')
            ->add('content', null, array('attr'=>array('class'=>'js-wysiwyg')))
            ->add('type', 'choice', array(
                'choice_list'=> new PostTypeChoiceList()
            ))
            ->add('categories', null, array(
                'expanded' => true,
                'multiple' => true
            ));
        
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PiotrOsinski\Bundle\Entity\Blog\Post'
        ));
    }

    public function getName()
    {
        return 'piotrosinski_blogbundle_blog_posttype';
    }
}

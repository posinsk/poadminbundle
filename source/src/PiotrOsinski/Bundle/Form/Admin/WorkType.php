<?php

namespace PiotrOsinski\Bundle\Form\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use PiotrOsinski\Bundle\Form\Admin\MediaType;

class WorkType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description')
            ->add('isActive')
            ->add('date');
        
        $builder->add('media', 'collection', array(
             'type'=>new MediaType(),
             'allow_add' => true,
             'allow_delete' => true,
             'by_reference' => false,
             'label'=>'Zdjęcia'
            ));
         
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PiotrOsinski\Bundle\Entity\Work'
        ));
    }

    public function getName()
    {
        return 'piotrosinski_bundle_worktype';
    }
}

<?php

namespace PiotrOsinski\Bundle\Form\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PiotrOsinski\Bundle\Entity\Blog\Category'
        ));
    }

    public function getName()
    {
        return 'piotrosinski_blogbundle_blog_categorytype';
    }
}

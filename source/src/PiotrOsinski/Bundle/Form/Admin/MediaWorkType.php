<?php

namespace PiotrOsinski\Bundle\Form\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MediaWorkType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('file', 'file',array(
            'label' => 'Zdjęcie',
            'required' => false,
            'attr' => array(
                'accept' => 'image/*',
            )));
        $builder->add('work');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PiotrOsinski\Bundle\Entity\Media'
        ));
    }

    public function getName()
    {
        return 'mediatype';
    }
}

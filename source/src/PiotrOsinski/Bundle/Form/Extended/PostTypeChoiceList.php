<?php
namespace PiotrOsinski\Bundle\Form\Extended;

use Symfony\Component\Form\Extension\Core\ChoiceList\LazyChoiceList;
use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;
use Symfony\Component\Form\Extension\Core\ChoiceList\SimpleChoiceList;
use PiotrOsinski\Bundle\Entity\Blog\Post;

class PostTypeChoiceList extends LazyChoiceList 
{
    protected function loadChoiceList()
    {
        $list = Post::getTypesList();

        return new SimpleChoiceList($list);

    }
}
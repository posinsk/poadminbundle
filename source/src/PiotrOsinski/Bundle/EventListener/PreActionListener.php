<?php

namespace PiotrOsinski\Bundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

use Doctrine\Common\Cache\ApcCache;

class PreActionListener
{
 
     protected $container;
 
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
    
    public function onKernelController(FilterControllerEvent $event)
     {
           $controller = $event->getController();
           if (!is_array($controller)) {
                // not a object but a different kind of callable. Do nothing
                return;
            }
            
            $controllerObject = $controller[0];
            $controllerObject->apcCache = new ApcCache();
            $controllerObject->apcCache->setNamespace('blog::');

            if ($controllerObject instanceof InitializableControllerInterface) {
                   $controllerObject->initialize($event->getRequest());
                   // this method is the one that is part of the interface.
            }
     }
}
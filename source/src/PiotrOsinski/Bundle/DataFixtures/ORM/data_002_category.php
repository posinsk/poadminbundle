<?php

namespace PiotrOsinski\Bundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use PiotrOsinski\Bundle\Entity\Blog\Category;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class data_002_category extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{
  
    public function load(ObjectManager $manager)
    {
        $names = array('Symfony2', 'PHP', 'jQuery', 'JavaScript', 'HTML', 'CSS', 'MySql', 'PostgreSql');
        $posts = range(0,12);
        foreach($names as $name){
            $cat = new Category;
            $cat->setName($name);
            
            shuffle($posts);
            for($i=0;$i<=rand(1,3);$i++){
                $post = $this->getReference('post'.$posts[$i]);
                $post->addCategorie($cat);
            }
            
            $manager->persist($cat);
            $manager->persist($post);
        }
        $manager->flush();
    }
    
    
    public function getOrder() {
        return 2;
    }
}


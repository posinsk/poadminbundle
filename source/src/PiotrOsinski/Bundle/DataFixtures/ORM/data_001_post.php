<?php

namespace PiotrOsinski\Bundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use PiotrOsinski\Bundle\Entity\Blog\Post;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class data_001_post extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{
    public $title = 'Suspendisse in vestibulum quam. Ut vitae libero non est euismod sodales ut a urna. Integer tempor sagittis elit non accumsan. Nullam aliquam auctor purus, eget laoreet lorem facilisis ut. Pellentesque id porta orci. Donec eget nibh turpis. In convallis nisl vel nisl vestibulum tempus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.';

 public $data = array('<p>If you are a "connected" developer, you are probably aware of the major
vulnerabilities found in Ruby on Rails recently. To be fair, weve also found
some serious issues in the Symfony code during the last few months.</p>

<p>As security management should be a top most priority for us and our customers,
Ive recently worked on improving the current situation in the Symfony world, with an
<a href="http://symfony.com/blog/security-issue-management-improvements">enhanced security process</a>.
But security management is also a very important topic for me because Symfony is
quickly growing in popularity for both end-user projects and Open-Source ones; and more
exposure also means more interest from the "bad guys".</p>

<p>One of the goal of good security issues management is <strong>transparency</strong>. Thats
why the Symfony project has a simple way of reporting security issues (via the
security [at] symfony.com email address), an easily accessible list of
<a href="http://symfony.com/security#security-advisories">security advisories</a>, and
a well defined blog post template to announce security issues. Recently, we
have also enforced the need to have a <a href="http://cve.mitre.org/cgi-bin/cvekey.cgi?keyword=symfony">CVE identifier</a>
for all security issues to help the broader community to be aware of Symfony security issues.</p>

<p>The best advice one can give you is to upgrade your favorite libraries as soon
as possible when new versions with security fixes are released. Easier say
than done.</p>'
,'<p>How do you know that a new release is out? How do you know that it contains
security fixes? For Symfony, you can subscribe to the RSS feed of the Symfony
<a href="http://feeds.feedburner.com/symfony/blog">blog</a>, or you can have a look at
our security advisories from time to time. But sometimes, thats not possible
as the project does not even have a blog. Was the security fix announced on
Twitter? Quite possible. But most of the time, smaller libraries just release
a new version without any proper security announcement.</p>

<p>But I want to provide a simple and efficient way to check for vulnerabilities
in a project and I want to serve more than just the Symfony community. Thats
why Im really proud to announce a new <a href="http://sensiolabs.com/">SensioLabs</a>
initiative: a simple way to check if your project depends on third-party
libraries with known security issues. The website explains how it works in
details
(<a href="https://security.sensiolabs.org/">https://security.sensiolabs.org/</a>), but
basically, this initiative gives you several ways to check for security issues
in your project dependencies based on the information contained in you
<code>composer.lock</code> file (you are using Composer to manage your dependencies,
right?):</p>

<ul>
<li><p>The <strong>website</strong> itself allows you to upload a <code>composer.lock</code> to check for
vulnerabilities;</p></li>
<li><p>A <strong>web service</strong> can used with curl or to integrate that tool into your
own continuous integration process (it returns its results as plain text or
as a JSON array);</p></li>
<li><p>A <strong>command line tool</strong> gives you the same feature as the web service and
the website but nicely packaged as a simple Symfony command.</p></li>
</ul>

<p>Of course, the most important part of this initiative is the database where
known security vulnerabilities are referenced. The database is hosted on
Github: <a href="https://github.com/sensiolabs/security-advisories">https://github.com/sensiolabs/security-advisories</a>.
We have already referenced known vulnerabilities for Symfony, Zend Framework,
and some well-known Symfony bundles, but the idea is for the community to
help us add more libraries and more importantly to update the database whenever a security issue is fixed in a library.
Even if you dont have a way to easily announce your security fix to the world, at least,
reference it in the database; contributing to the database is really easy: fork
the repository, contribute your changes, and send a pull request (you can even do everything from the Github web interface if you want).</p>

<p><a href="https://security.sensiolabs.org/check">Check</a> your projects, upgrade your
dependencies when needed, and
<a href="https://github.com/sensiolabs/security-advisories">contribute</a> to the
database!</p>',
'<p>Yesterday, Zend Framework 2.0 and Symfony 2.1 were released... almost at the
same time. First, I want to congratulate the Zend Framework team for this huge
milestone; I know that working on a new major version is no small task.</p>

<p>And of course, people started to ask questions about these new versions and
one of the most popular was: "Why would I choose framework X over framework
Y?". As you can imagine, the answer is not that easy as each framework has its
own specificities.</p>

<p>I like to think that most popular frameworks are modular enough, fast enough,
well-documented enough; they are using well-known design patterns. So, besides
the buzzwords, what is unique to Symfony? Why would you want to use Symfony
instead of X (replace X with ZF, CakePHP, Lithium, Laravel, Slim, Yii, you name it)? Thats the question Im going to answer in this post.</p>

<p>So, without further ado, here are my main selling points for Symfony:</p>',
'<ul><li><p>Symfony is not a framework but a project. Depending on your needs, you can
choose to use some of the <a href="http://symfony.com/components">Symfony
Components</a>, the
<a href="http://silex.sensiolabs.org/">Silex</a> micro-framework, or the full-stack
framework.</p></li>
<li><p>Symfony is used by many large companies (like the BBC or CBS), by many large websites (like <a href="http://www.ted.com/">TED</a>, <a href="http://wetter.com/">wetter.com</a>, <a href="http://lockerz.com/">Lockers</a> or even YouPorn just to name a few) and some Open-Source projects are
also powered by Symfony (CMSes like
<a href="http://symfony.com/blog/symfony2-meets-drupal-8">Drupal</a> or
<a href="http://symfony.com/blog/symfony2-meets-ez-publish-5">eZpublish</a>, libraries
like <a href="https://github.com/sebastianbergmann/phpunit/">PHPUnit</a> or
<a href="http://www.doctrine-project.org/">Doctrine</a>, products like
<a href="https://www.phpbb.com/">phpBB</a> or <a href="http://www.shopware.de/">shopware</a>, and even frameworks like
<a href="http://www.ppi.io/">PPI</a> or <a href="http://laravel.com/">Laravel</a>). This brings a lot of interoperability between all these solutions.</p></li>
<li><p>Symfony enjoys a huge <a href="http://connect.sensiolabs.com/">community</a> of users
and <a href="http://symfony.com/contributors">contributors</a>; during the last year
alone, 550+ people contributed to the Symfony core and the community
created over 1,600 <a href="http://knpbundles.com/">bundles</a> for the full-stack
framework. Symfony also has several annual dedicated
<a href="http://live.symfony.com/">conferences</a> around the world and a large number
of user <a href="http://symfony.com/events">groups</a>.</p></li>
<li><p>Symfony has been created in 2005 and here to stay. Besides
<a href="http://sensiolabs.com/">SensioLabs</a>, many other companies rely on Symfony
for their clients and they contribute, invest money, and sponsor the future
of the project.</p></li>
<li><p>Symfony embraces the "dont reinvent the wheel" philosophy, and provides
tight integration with many other Open-Source projects (like Monolog,
Assetic, Doctrine, Propel, ...).</p></li>
<li><p>Symfony tries to bring innovation to PHP: it was one of the first major
frameworks to embrace PHP 5.3, to introduce the usage of a Dependency
Injection container, and to use a templating engine for its templates by
default, <a href="http://twig.sensiolabs.org/">Twig</a>, which is now also adopted by
major CMSes like Drupal and eZpublish. Symfony also has some unique
features like its gorgeous debug toolbar and its great built-in profiler.</p></li>
</ul>',
'<p>If you think Ive missed some important selling points, feel free to list them
in the comments and in a few days, I will aggregate everything in a new page
that will be hosted on symfony.com to help users make an informed decision
when choosing a framework.</p>

<p>And if other frameworks would like to do the same, I would happily add links
to their pages on the Symfony one, and if in turn, they do the same, that
would create a ring of links that will ease the choice of a PHP framework for
developers.</p>',
'<p>Rants about PHP are everywhere, and they even come from smart guys. When Jeff
Atwood wrote yet another
<a href="http://www.codinghorror.com/blog/2012/06/the-php-singularity.html">rant</a>
about PHP, it made me think about the good parts of PHP.</p>

<p>The biggest problem of these rants is that they come from people stuck in the
old days of PHP. They either dont care or they dont want to admit that PHP
actually evolves at a very fast pace, both at the language level but also at
the community level. In fact, it evolves much faster than any other language
or web platform. It has not always been the case, but the last 5 years have
been an amazing journey for PHP.</p>',
'<p>Before talking about the amazing things the PHP community has achieved
recently, lets have a look at some interesting numbers: PHP is used by
<a href="http://w3techs.com/technologies/overview/programming_language/all">77.9%</a> of
all the websites whose server-side programming language is known. WordPress is
used by
<a href="http://w3techs.com/technologies/overview/content_management/all">16.6%</a> of
<strong>all the websites</strong> in the world. If you have a look at the top three CMSes,
for the websites that use a monitored content management system: Wordpress is
first with 54.3%, Joomla is second with 9.2%, and Drupal is third with 6.8%.
Three products written in PHP.</p>

<p>PHP must have done something right, no?</p>

<p>Now, let me tell you a secret, the PHP "tour de force": Despite the changes
over the years, PHP is still the <strong>easiest language</strong> to learn for
non-technical people: it allows anyone to create dynamic websites faster than
with any other technologies, it allows anyone to host websites cheaply and
without any hassles. PHP is probably not the best designed language in the
world, but it lets you <strong>get things done</strong>, and you cant argue with that.</p>

<h2>PHP, the Language</h2>

<p>PHP 5.0 (released in 2004) brought us a very solid object model... wait a
minute, Im talking about something released almost 8 years ago. Fast forward
now. The latest PHP release, PHP 5.4, comes with all the bells and whistles
you might dream of in a modern web language: yes, PHP supports namespaces;
yes, PHP supports closures; yes, PHP supports traits.</p>

<p>It took some time, but PHP 5.4 also comes with some nice syntactic sugar that
makes the whole experience better than ever: yes, PHP supports <code>[]</code> to define
arrays; yes, PHP supports calling a method on a newly created object (<code>(new
Foo())-&gt;bar()</code>); yes, PHP supports getting an array item from any expression
(<code>$foo-&gt;bar()[1]</code>).</p>

<p>PHP has even learned from its mistakes: <code>register_globals</code> and <code>magic_quotes</code>
are definitely gone.</p>',
'<p>Last, but not the least, PHP even comes with a built-in web server that eases
local testing... and it starts in a matter of micro-seconds.</p>

<p><em>Next challenges</em>: How do we "upgrade" all the old tutorials talking about PHP
on the web? What is the best way to support the WebSocket technology in a PHP
application?</p>

<h2>PHP, the Ecosystem</h2>

<p>Having a good language is great, but having a great ecosystem is even better.
And the PHP ecosystem has evolved a lot in the last few years.</p>

<h3>Git</h3>

<p>I wont talk too much about this one. Git is everywhere and PHP embraced Git
pretty fast. Almost all major PHP libraries, frameworks, and products are now
using Git, including PHP itself.</p>

<h3>Composer</h3>

<p>Two years ago, I wanted to get rid of my ugly-PEAR-hack I did in symfony 1 to
support plugins. I wanted to replace it with something that was able to manage
project dependencies, not a global installer like PEAR. Managing dependencies
is not an easy task, so I tried to find the best algorithm to manage software
dependencies; I had a look at everything: from Perl to Ruby, from Debian to
Redhat. Nothing was satisfactory: only homegrown solutions that happen to
work... empirically. Then, I stumbled upon
<a href="http://en.wikipedia.org/wiki/ZYpp">ZYpp</a>. That was it. ZYpp uses a
<a href="http://en.wikipedia.org/wiki/Boolean_satisfiability_problem">SAT</a> solver to
manage dependencies. Fast forward. Thanks to the hard work of
<a href="http://www.naderman.de/">Nils</a> Adermann and <a href="http://seld.be/">Jordi</a>
Boggiano, PHP now has one of the best dependency manager,
<a href="http://getcomposer.org/">Composer</a>.</p>

<p>Yes, PHP has a better dependency manager than any other languages.</p>

<p>And thanks to Git, Composer, and the PHP built-in web server, it has never
been <a href="https://twitter.com/fabpot/status/220254879459975169">easier</a> to
download/install/test a PHP project.</p>

<p>Want to test Symfony (using PHP 5.4)?</p>

<pre class="command-line"><code>$ composer.phar create-project symfony/framework-standard-edition
$ cd framework-standard-edition
$ ./app/console server:run
</code></pre>',
'<p>Want to test Silex?</p>

<pre class="command-line"><code>$ composer.phar create-project fabpot/silex-skeleton
$ cd silex-skeleton
$ php -S localhost:8888 -t web/
</code></pre>

<p>Dont know Composer yet? You should learn about it. Browse
<a href="http://packagist.org/packages/">Packagist</a>, the main Composer repository: it
already has 1900+ packages available and they have been installed more than a million times
in less than 3 months.</p>

<p><em>Next challenge</em>: include the Composer installer in the next PHP version?</p>

<h3>Collaboration</h3>

<p>Community collaboration is the most important point of this post; the one Im
the most proud of. We start to see better collaboration between PHP projects,
even from the very big ones, the ones you would think are large enough to not
care about the others.</p>

<p>phpBB, <a href="http://symfony.com/blog/symfony2-meets-drupal-8">Drupal</a>, <a href="http://symfony.com/blog/symfony2-meets-ez-publish-5">eZ Publish</a>, Symfony, and many others (phpDocumentor, PHPUnit,
Behat, Zikula, Propel, Doctrine, Midgard, ...) are <strong>sharing</strong> code. Yes, they
are "competitors" but they all understood that cross-pollination was a good
thing. And Composer is a good enabler.</p>

<p><em>Next challenge</em>: Convince even more projects to join the trend.</p>

<h2>Conclusion</h2>

<p>Let me say it again: PHP is probably not the best language out there, and Im
the first one to scream about its quirks, but <strong>PHP is the best web
platform</strong>... ever.</p>',
'<p>As a lead developer of Symfony, I need feedback. Symfony being an Open-Source
project, there is no way I can know how many people are using it, who they are
and what they do with it. Thats fine and all, but sometimes, that would
really help me to know the Symfony user base a bit more. Sure enough, I have
some feedback via email and Twitter. Actually, I receive tons of
emails every single day about Symfony and my other Open-Source projects (with a raising number of Git pull requests... keep them
coming by the way). But unfortunately, most of them are from
people complaining about something or asking me some more free stuff. I must
admit that this is depressing.</p>

<p>During the Symfony Day conference last week, I suggested that people send me a
postcard if they use Symfony. The idea is not mine, as
<a href="http://www.derickrethans.nl/xdebug-2-released.html">Derick</a> did the same a
couple of years ago for XDebug.</p>

<p>So, if you use Symfony and enjoy it, consider sending me a postcard. Tell me
why you are a Symfony user and what you do with it (if youre not comfortable with English, thats fine... I will also enjoy Japanese, Chinese, or Arabic postcards). You can send them to my
personal address:</p>
<p>And I have a dream that one day I will receive a postcard each time a new
Symfony website is deployed to production.</p>',
'<p>This has been an amazing week for me,
<a href="http://www.symfony-project.org/">Symfony</a>, and the whole Symfony community.
With more than 350 attendees coming from more than 30 countries, the Symfony
Live <a href="http://www.symfony-live.com/">conference</a> was a blast. I want to thank
all attendees, speakers, and the <a href="http://www.sensiolabs.com/">Sensio Labs</a>
team for coming to the conference and making it a truly fantastic event.</p>

<p>If you have not attended the conference, you can still come to the Symfony
Live
<a href="http://www.meetup.com/London-events-by-Boxlight-Media/calendar/12416585/">After-Party</a>
next week in London. And dont miss Symfony
<a href="http://www.symfonycamp.com/">Camp</a> in June and Symfony
<a href="http://www.symfonyday.com/en/">Day</a> in October. If you live far away from
Europe, stay tuned as we have more exciting news to announce in the coming
weeks.</p>

<p>The conference was also the occasion to unveil the first preview release of
the upcoming <a href="http://www.symfony-reloaded.org/">Symfony 2</a> framework. Since
then, the buzz has been great and I have already received a lot of good
comments on it. Of course, I hope that this is just the beginning, and that
more people will provide feedback.</p>',
'<h2>On the PHP Community</h2>

<p>Im also very proud that people from so many different PHP "sub-communities"
attended the conference: Jon Wage (Doctrine project manager and lead developer of Doctrine 1.x), François
Zaninotto (lead developer of Propel), Nils Adermann (lead developer of PhpBB),
and of course, Matthew Weier OPhinney (Zend Framework lead).</p>

<p>I like to see the whole PHP community as a big unified group of developers
trying to promote the same platform. And I do my best to be a good "PHP
citizen". I try to innovate by adapting concepts from other languages. But I
also try to not reinvent the wheel when its not mandatory. The symfony 1
framework uses a lot of third-party libraries like Propel and Doctrine. And
for Symfony 2, Im proud to have replaced our own Logger and Cache system by
the equivalent components from the Zend Framework. And you can imagine that it
was not an easy decision to take. First, because it meant throwing away code
we have written and maintained for years. Then, because the Zend Framework is
our main competitor. But Im sure we have made the right decision and Matthew
seems to have the same 
<a href="http://weierophinney.net/matthew/archives/232-Symfony-Live-2010.html">point of view</a>
 on that matter.</p>',
'<h2>On PHP Frameworks</h2>

<p>People are jealous, so jealous that it makes me really sad. For the last few
years, some PHP framework communities attempted to "kill" Symfony by trying to
demonstrate that it is too slow, too complex, and too bloated. Of course,
thats not true. And the number of
<a href="http://answers.yahoo.com/">high</a>-<a href="http://delicious.com/">traffic</a>
<a href="http://www.dailymotion.com/">websites</a> using symfony 1 speaks for us.</p>

<p>I get inspiration from many different frameworks from many different
languages. Besides PHP projects, I like to follow the development of Django,
Spring, Rails, Maven, Jinja, Rack, Sinatra, and many others. I try to bring
innovation to the table. And for Symfony 2, I have tried to take into account
the major pain points of symfony 1. The truth is that Symfony 2.0 requires no
configuration. The truth is that Symfony 2 is really fast. Is it the fastest
framework? I dont care. It is probably fast enough for your next website, and
so is symfony 1.</p>

<p>Now if some people cannot accept that Symfony 2 is really fast and if they
cannot accept that Symfony 2 is great step forward for PHP, thats sad. The
web evolves very fast. Competition is everywhere for PHP. We should all be in
the same boat.</p>

<p>I would love if people from different framework communities can work together
more often, like what we have done with the PHP 5.3 interoperability group. I
would love if we can share more components. I would love to discuss how we can
make our PHP community grow faster.</p>

<p>Is it a dream? I hope its not. And I have a proposal. Lets organize an event
where several PHP framework communities can discuss and share ideas. Anyone?</p>

<h2>On Dependency Injection</h2>

<p>The biggest Symfony 2 innovation lies in its low level architecture. And its
flexibility and speed is mainly due to the Symfony Dependency Injection
Container component. Dependency Injection Containers are not widespread in the
PHP world, and I really think that this is a very good approach. Thats why I
talk about Dependency Injection at conferences. Next time I will talk about
this topic is during the
<a href="http://confoo.ca/en/2010/session/dependency-injection-in-php-5-2-and-5-3">ConFoo</a>
conference in March.</p>');

 
  
    

    public function load(ObjectManager $manager)
    {
        $title = explode(' ', $this->title);
        
        foreach($this->data as $k =>$content){
            $post = new Post;
            $post->setContent($content);
            $post->setType(1);
            
            $post->setExcerpt(substr(strip_tags($content),0, 300));
            
            if($k >=8){
                shuffle($title);
                $nTitle = null;
                for($i=0; $i<=rand(5,15); $i++){
                    $nTitle[] = $i == 0 ? ucfirst($title[$i]) : preg_replace('/\\./','',strtolower($title[$i])); 
                }
                $post->setTitle(implode(' ', $nTitle).'.');
            }else{
                $titles = array(
                    'Full text search with Symfony2, Doctrine and PostgreSQL, part I (indexes, indexing rows)',
                    'Full text search with Symfony2, Doctrine and PostgreSQL, part II (querying results)',
                    'Custom functions in Symfony2 & Doctrine',
                    'Custom data types in Symfony2 & Doctrine',
                    'Doctrine2 & Object mapping (RSM)',
                    'Using PH APC key-value cache in Symfony2',
                    'Using random order with Doctrine2',
                    'Caching Doctrine2 results',
                    'Use Symfony2 session without cookies'
                    );
                $post->setTitle($titles[$k]);
            }
            
            
            $manager->persist($post);
            $this->addReference('post'.$k, $post);
        }
        $manager->flush();
    }
    
    public function getOrder() {
        return 1;
    }
}



$(document).ready(function(){
   
   $('.js-redactor').redactor({
        lang: 'pl',
        imageUpload:'/backend.php/wyslij',
        shortcuts: false,
        convertLinks: true
    });
    
    if($('.js-no-prettyprint').length < 1)
        prettyPrint();
    
});
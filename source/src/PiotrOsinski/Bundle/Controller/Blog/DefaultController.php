<?php

namespace PiotrOsinski\Bundle\Controller\Blog;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Doctrine\Common\Cache\ApcCache;

use PiotrOsinski\Bundle\Entity\Blog\Post;

/**
 * @Route("/blog")
 */
class DefaultController extends Controller {
    
    /**
     * @Route(name="empty")
     * @Template()
     */
    public function emptyAction(Request $request) {
        return array('argument1'=>$request->get('argument1'),'argument2'=>$request->get('argument2'));
    }

    /**
     * @Route("/{page}", name="posts_list", defaults={"page"=1})
     * @Route("/category/{categorySlug}/{page}", name="posts_by_category", defaults={"page"=1})
     * @Template()
     */
    public function indexAction(Request $request, $page) {
        $d = $this->getDoctrine();
        $cacheKey = 'post_page:'.$page;

        $q = $d->getRepository('PiotrOsinskiBundle:Blog\Post')
                ->createQueryBuilder('p')
                ->select('p, c')
                ->leftJoin('p.categories', 'c')
                ->where('p.type = :type')
                ->setParameter(':type', 1)
                ->orderBy('p.createdAt', 'DESC');
        
        //if filtering by category was selected
        if ($request->get('_route') == 'posts_by_category'){
            $categorySlug = $request->get('categorySlug');
            
            $categoryPostsKey = 'category_posts_ids:'.$categorySlug;
            if(!$this->apcCache->contains($categoryPostsKey)){
                $postsIds = $d->getRepository('PiotrOsinskiBundle:Blog\Category')
                        ->createQueryBuilder('c')
                        ->select('p.id')
                        ->leftJoin('c.posts' ,'p')
                        ->where('c.slug = :categorySlug')
                        ->setParameter('categorySlug', $categorySlug)
                ->getQuery()->getResult();

                array_walk($postsIds, function(&$val){
                    return $val = $val['id'];
                });
                $this->apcCache->save($categoryPostsKey, $postsIds);
            }else
                $postsIds = $this->apcCache->fetch($categoryPostsKey);
            
            $q->andWhere('p.id IN (:postsFromCategory)')
                    ->setParameter('postsFromCategory', $postsIds);
            
            $cacheKey .= ':'.$categorySlug;
        }   
        
        if(!$this->apcCache->contains($cacheKey)){
            $paginator = $this->get('knp_paginator');
            $pager = $paginator->paginate(
                    $q, $request->get('page', 1)/* page number */, 5/* limit per page */
            );
            $this->apcCache->save($cacheKey, $pager);
        }else
            $pager = $this->apcCache->fetch($cacheKey);

        return compact('pager');
    }
    
    /**
     * @Route(name="categories")
     * @Template("PiotrOsinskiBundle:Blog:Default\partials\categories.html.twig")
     */
    public function categoriesAction($categorySlug = null){
        
        $categories = $this->getDoctrine()
                    ->getRepository('PiotrOsinskiBundle:Blog\Category')
                    ->createQueryBuilder('c')
                    ->getQuery()
                    ->useResultCache(true, null, 'categories')
                    ->getResult();

        
        return compact('categories', 'categorySlug');
    }
    
    /**
     * @Route(name="similar_posts")
     * @Template("PiotrOsinskiBundle:Blog:Default\partials\similarPosts.html.twig")
     * @ParamConverter("post", class="PiotrOsinskiBundle:Blog\Post")
     */
    public function similarPostsAction(\PiotrOsinski\Bundle\Entity\Blog\Post $post){
        
        $ids = array();
        foreach($post->getCategories() as $category){
            $ids[] = $category->getId();
        }
        
        if(!empty($ids)){
            $d = $this->getDoctrine();
            $posts = $d->getRepository('PiotrOsinskiBundle:Blog\Post')
                    ->createQueryBuilder('p')
                    ->select('c, p, RANDOM() * p.id as rnd')
                    ->leftJoin('p.categories', 'c')
                    ->where('c.id IN (:ids)')
                    ->setParameter(':ids', $ids)
                    ->andWhere('p.id != :id')
                    ->setParameter(':id', $post->getId())
                    ->orderBy('rnd', 'DESC')
                    ->setMaxResults(3)
                    ->getQuery()
                    ->getResult();
            
            array_filter($posts, function(&$val){
                return $val = $val[0];
            });
            
        }
        else
            $posts = array();
            
        return compact('posts');
    }
    
    /**
     * @Route("/post/{id}/{slug}", name="post")
     * @Template()
     */
    public function postAction(Request $request, $id, $slug){
        
        $postCacheKey = Post::generateCacheKey($id, $slug);

            try{
                $post = $this->getDoctrine()->getRepository('PiotrOsinskiBundle:Blog\Post')
                    ->createQueryBuilder('p')
                    ->select('p, c')
                    ->leftJoin('p.categories', 'c')
                    ->where('p.id = :id')
                    ->andWhere('p.slug = :slug')
                    ->andWhere('p.type = :type')
                    ->setParameter(':id', $id)
                    ->setParameter(':slug', $slug)
                    ->setParameter(':type', 1)
                    ->getQuery()
                    ->useResultCache(true, null, $postCacheKey)
                    ->getSingleResult();
            }catch(\Doctrine\ORM\NoResultException $e){
                //if one not finds the post -> search for similar posts
                $post = $this->searchSimilarPost($id, $slug);
                    if($post instanceof \PiotrOsinski\Bundle\Entity\Blog\Post){
                        return $this->redirect($this->generateUrl ('post', array(
                            'id' => $post->getId(),
                            'slug' => $post->getSlug()
                                )), 301);
                    }else{
                        $content = $this->renderView('PiotrOsinskiBundle:Blog:Default\postNotFound.html.twig', array(
                            'posts' => $post
                            ));
                        return new Response($content);
                    }
            }

            return compact('post');
    }
    
    
    private function searchSimilarPost($id, $slug){
        $d = $this->getDoctrine();
        
        $post = $d->getRepository('PiotrOsinskiBundle:Blog\Post')->find($id);
        
        //maybe slug was misspelled?
        if($post){
            return $post;
        }
        
        //fetch all posts and search, because id or slug had huge mistake!
        $posts = $d->getRepository('PiotrOsinskiBundle:Blog\Post')
                ->createQueryBuilder('p')
                ->select('p', 'c')
                ->leftJoin('p.categories', 'c')
                ->getQuery()->getResult();
        
        $similarPosts = array();
        foreach($posts as $post){
            similar_text($slug, $post->getSlug(), $percent);
            if($percent >= 65){
                $nKey = (int)round(($percent * 1000), 0);
                if(!array_key_exists($nKey, $similarPosts))
                    $similarPosts[$nKey] = $post;
                else $similarPosts[$nKey + rand(0,100)] = $post;
            }
        }

        //sort them by similarity from most similar to the less similar
        ksort($similarPosts, SORT_NUMERIC);
        $similarPosts = array_reverse($similarPosts, true);
        return empty($similarPosts) ? false : $similarPosts;            
        
    }

}

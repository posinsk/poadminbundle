<?php

namespace PiotrOsinski\Bundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * @Route("/admin")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/login", name="admin_login")
     * @Template()
     */
    public function loginAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                SecurityContext::AUTHENTICATION_ERROR
            );
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

           return array(
                'last_username' => $session->get(SecurityContext::LAST_USERNAME),
                'error'         => $error,
        );
        
    }
    
    /**
     * @Route("/", name="admin_index")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }

    /**
     * @Route("/example", name="admin_example")
     * @Template()
     */
    public function exampleAction()
    {
        return array();
    }
    
    
    /**
     * @Route(name="admin_menu")
     * @Template("PiotrOsinskiBundle:Admin\Default:_menu.html.twig")
     */
    public function menuAction()
    {
        $controller = $this->getRequest()->get('controller');
//        var_dump($controller);die();
        $controllerPath = explode('\\', $controller);
        $controllerName = explode('::', $controllerPath[4]);
        $controllerName = preg_replace('/Controller/', '', $controllerName);
//        var_dump($controllerName);
        return array('controller'=>$controllerName[0]);
    }
    
    
    /**
     * @Route("/logout", name="admin_logout")
     * @Template()
     */
    public function logoutAction()
    {
        return array();
    }
}

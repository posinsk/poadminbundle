<?php

namespace PiotrOsinski\Bundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use PiotrOsinski\Bundle\Entity\Blog\Post;
use PiotrOsinski\Bundle\Form\Admin\PostType;

/**
 * Blog\Post controller.
 *
 * @Route("/admin/post")
 */
class PostController extends Controller
{
    /**
     * Lists all Blog\Post entities.
     *
     * @Route("/list/{type}", name="admin_post", defaults={"type"=0})
     * @Method("GET")
     * @Template()
     */
    public function indexAction($type)
    {
        $em = $this->getDoctrine()->getManager();

        $q = $em->getRepository('PiotrOsinskiBundle:Blog\Post')
                ->createQueryBuilder('p')
                ->select('p', 'c')
                ->leftJoin('p.categories', 'c')
                ->where('p.type = :type')
                ->setParameter(':type',$type);
        
        $paginator  = $this->get('knp_paginator');
        $pager = $paginator->paginate(
            $q,
            $this->get('request')->query->get('page', 1)/*page number*/,
            20/*limit per page*/
        );
        
        $postTypes = Post::getTypesList();
        
        $ptc = $em->getRepository('PiotrOsinskiBundle:Blog\Post')
                ->createQueryBuilder('p')
                ->select('count(p), p.type')
                ->groupBy('p.type')
                ->getQuery()
                ->getResult();
        
        $typeCount = array();
        foreach($ptc as $type){
            $typeCount[$type['type']] = $type[1];
        }
        
        return compact('pager', 'postTypes', 'typeCount');
        }

    /**
     * Creates a new Blog\Post entity.
     *
     * @Route("/create-post", name="admin_post_create")
     * @Method("POST")
     * @Template("PiotrOsinskiBundle:Admin\Post:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity  = new Post();
        $form = $this->createForm(new PostType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_post_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Blog\Post entity.
     *
     * @Route("/new", name="admin_post_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Post();
        $form   = $this->createForm(new PostType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Blog\Post entity.
     *
     * @Route("/{id}", name="admin_post_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PiotrOsinskiBundle:Blog\Post')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Blog\Post entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Blog\Post entity.
     *
     * @Route("/{id}/edit", name="admin_post_edit")
     * @Method("GET")
     * @Template("PiotrOsinskiBundle:Admin\Post:new.html.twig")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PiotrOsinskiBundle:Blog\Post')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Blog\Post entity.');
        }

        $editForm = $this->createForm(new PostType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Blog\Post entity.
     *
     * @Route("/{id}", name="admin_post_update")
     * @Method("PUT")
     * @Template("PiotrOsinskiBundle:Admin\Post:new.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PiotrOsinskiBundle:Blog\Post')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Blog\Post entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new PostType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_post_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Blog\Post entity.
     *
     * @Route("/{id}", name="admin_post_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('PiotrOsinskiBundle:Blog\Post')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Blog\Post entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_post'));
    }

    /**
     * Creates a form to delete a Blog\Post entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}

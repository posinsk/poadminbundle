<?php

namespace Alliance\CompetitionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Alliance\CompetitionBundle\Entity\Competitor;
use Alliance\CompetitionBundle\Form\CompetitorType;

/**
 * Competitor controller.
 *
 */
class CompetitorController extends Controller
{
    /**
     * Lists all Competitor entities.
     *
     * @Route("/", name="alliance_competitor")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AllianceCompetitionBundle:Competitor')
                ->findBy(array('user'=>$this->getUser()->getId()
                ,'deleted'=>false));

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new Competitor entity.
     *
     * @Route("/dodaj-zawodnika-post", name="alliance_competitor_create")
     * @Method("POST")
     * @Template("AllianceCompetitionBundle:Competitor:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity  = new Competitor();
        $form = $this->createForm(new CompetitorType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $entity->setUser($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('alliance_competitor'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Competitor entity.
     *
     * @Route("/dodaj-zawodnika", name="alliance_competitor_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Competitor();
        $form   = $this->createForm(new CompetitorType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Competitor entity.
     *
     * @Route("/zawodnik/{id}/edit", name="alliance_competitor_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AllianceCompetitionBundle:Competitor')->find($id);

        if (!$entity || $entity->getUser()->getId() != $this->getUser()->getId()) {
            throw $this->createNotFoundException('Unable to find Competitor entity.');
        }

        $editForm = $this->createForm(new CompetitorType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Competitor entity.
     *
     * @Route("/edytuj/{id}", name="alliance_competitor_update")
     * @Method("PUT")
     * @Template("AllianceCompetitionBundle:Competitor:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AllianceCompetitionBundle:Competitor')->find($id);

        if (!$entity || $entity->getUser()->getId() != $this->getUser()->getId()) {
            throw $this->createNotFoundException('Unable to find Competitor entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new CompetitorType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('alliance_competitor_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Competitor entity.
     *
     * @Route("/usun-zawodnika/{id}", name="alliance_competitor_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AllianceCompetitionBundle:Competitor')
                    ->createQueryBuilder('c')
                ->where('c.id = :id')
                ->setParameter(':id', $id)
                ->getQuery()
                ->getResult();

            if (!$entity[0] || $entity[0]->getUser()->getId() != $this->getUser()->getId()) {
                throw $this->createNotFoundException('Unable to find Competitor entity.');
            }

            $entity[0]->setDeleted(true);
            $em->persist($entity[0]);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('alliance_competitor'));
    }

    /**
     * Creates a form to delete a Competitor entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}

<?php

namespace Alliance\CompetitionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * @Route("/")
 */
class DefaultController extends Controller {

    /**
     * @Route("/zaloguj", name="alliance_login")
     * @Template()
     */
    public function loginAction() {
        $request = $this->getRequest();
        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                SecurityContext::AUTHENTICATION_ERROR
            );
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

           return array(
                'last_username' => $session->get(SecurityContext::LAST_USERNAME),
                'error'         => $error,
        );
        
    }
    
    /**
     * @Route("/logout", name="alliance_logout")
     */
    public function logoutAction()
    {
        return array();
    }

    /**
     * @Route("/zarejestruj-ekipe", name="alliance_user_register")
     * @Template()
     */
    public function registerUserAction(Request $request) {
        $entity = new \Alliance\CompetitionBundle\Entity\Users();
        $form = $this->createForm(new \Alliance\CompetitionBundle\Form\UsersType(), $entity);

        if('POST' == $request->getMethod()){
            $form->bind($request);

            if ($form->isValid()) {
                
                $factory = $this->container->get('security.encoder_factory');
                $encoder = $factory->getEncoder($entity);
                $password = $encoder->encodePassword($entity->getPassword(), $entity->getSalt());
                $entity->setPassword($password);
                
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();

                return $this->redirect($this->generateUrl('alliance_login'));
            }
        }
        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

}

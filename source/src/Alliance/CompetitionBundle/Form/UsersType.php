<?php

namespace Alliance\CompetitionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UsersType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstName', null, array('label'=>'Imię'));
        $builder->add('lastName', null, array('label'=>'Nazwisko'));
        $builder->add('email', null, array('label'=>'Email'));
        $builder->add('team', null, array('label'=>'Nazwa drużyny'));
        $builder->add('password', 'repeated', array(
            'type' => 'password',
            'invalid_message' => 'Podane hasła są różne',
            'required' => true,
            'label' => '  ',
            'first_options'  => array('label' => 'Hasło'),
            'second_options' => array('label' => 'Powtórz hasło')
        ));

    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Alliance\CompetitionBundle\Entity\Users'
        ));
    }

    public function getName()
    {
        return 'userstype';
    }
}

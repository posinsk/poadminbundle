<?php

namespace Alliance\CompetitionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CompetitorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', null, array('label'=>'Imię', 'required'=>true))
            ->add('lastName', null, array('label'=>'Nazwisko', 'required'=>true))
            ->add('weight', null, array('label'=>'Kategoria', 'required'=>true));
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Alliance\CompetitionBundle\Entity\Competitor'
        ));
    }

    public function getName()
    {
        return 'alliance_competitionbundle_competitortype';
    }
}

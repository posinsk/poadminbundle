<?php

namespace PiotrOsinski\Bundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Alliance\CompetitionBundle\Entity\WeightCategory;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class data_001_weight extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{
    

    public function load(ObjectManager $manager)
    {
        $categories = array(
            'Początkujący -60 kg',
            'Początkujący -65 kg',
            'Początkujący -70 kg',
            'Początkujący -76 kg',
            'Początkujący -83 kg',
            'Początkujący -91 kg',
            'Początkujący -100 kg',
            'Początkujący +100 kg',
            'Średniozaawansowani -65,9 kg',
            'Średniozaawansowani -76,9 kg',
            'Średniozaawansowani -87,9 kg',
            'Średniozaawansowani -98,9 kg',
            'Średniozaawansowani +99 kg',
            'Zaawansowani -65,9 kg',
            'Zaawansowani -76,9 kg',
            'Zaawansowani -87,9 kg',
            'Zaawansowani -98,9 kg',
            'Zaawansowani +99 kg',
            'Kobiety -60 kg',
            'Kobiety +60 kg'
        );
        
        foreach($categories as $weight){
            $cat = new WeightCategory();
            $cat->setName($weight);
            $manager->persist($cat);
        }
        
        $manager->flush();
        
    }
    
    public function getOrder() {
        return 1;
    }
}


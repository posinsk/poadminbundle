<?php

namespace Alliance\CompetitionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Competitor
 *
 * @ORM\Table("competition_competitor")
 * @ORM\Entity
 */
class Competitor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotNull(message="Wypełnij to pole")
     * @ORM\Column(name="first_name", type="string", length=255)
     */
    private $firstName;

    /**
     * @var string
     * @Assert\NotNull(message="Wypełnij to pole")
     * @ORM\Column(name="last_name", type="string", length=255)
     */
    private $lastName;
    
    /**
     *
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted = false;

    /**
     * @ORM\ManyToOne(targetEntity="Users", inversedBy="competitors")
     */
    private $user;
    
    /**
     * @ORM\ManyToOne(targetEntity="WeightCategory", inversedBy="competitor")
     */
    private $weight;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Competitor
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    
        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Competitor
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    
        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set user
     *
     * @param \Alliance\CompetitionBundle\Entity\Users $user
     * @return Competitor
     */
    public function setUser(\Alliance\CompetitionBundle\Entity\Users $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Alliance\CompetitionBundle\Entity\Users 
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->weight = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setDeleted(false);
    }
    
    /**
     * Add weight
     *
     * @param \Alliance\CompetitionBundle\Entity\WeightCategory $weight
     * @return Competitor
     */
    public function addWeight(\Alliance\CompetitionBundle\Entity\WeightCategory $weight)
    {
        $this->weight[] = $weight;
    
        return $this;
    }

    /**
     * Remove weight
     *
     * @param \Alliance\CompetitionBundle\Entity\WeightCategory $weight
     */
    public function removeWeight(\Alliance\CompetitionBundle\Entity\WeightCategory $weight)
    {
        $this->weight->removeElement($weight);
    }

    /**
     * Get weight
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set weight
     *
     * @param \Alliance\CompetitionBundle\Entity\WeightCategory $weight
     * @return Competitor
     */
    public function setWeight(\Alliance\CompetitionBundle\Entity\WeightCategory $weight = null)
    {
        $this->weight = $weight;
    
        return $this;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return Competitor
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    
        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }
}
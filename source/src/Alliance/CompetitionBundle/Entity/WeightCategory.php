<?php

namespace Alliance\CompetitionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WeightCategory
 *
 * @ORM\Table("competition_weight_category")
 * @ORM\Entity
 */
class WeightCategory {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Competitor", mappedBy="weight", cascade={"persist","remove"})
     */
    
    private $competitor;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }
    
    public function __toString() {
        return $this->getName();
    }

    /**
     * Set title
     *
     * @param string $title
     * @return WeightCategory
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle() {
        return $this->title;
    }


    /**
     * Set name
     *
     * @param string $name
     * @return WeightCategory
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set competitor
     *
     * @param \Alliance\CompetitionBundle\Entity\Competitor $competitor
     * @return WeightCategory
     */
    public function setCompetitor(\Alliance\CompetitionBundle\Entity\Competitor $competitor = null)
    {
        $this->competitor = $competitor;
    
        return $this;
    }

    /**
     * Get competitor
     *
     * @return \Alliance\CompetitionBundle\Entity\Competitor 
     */
    public function getCompetitor()
    {
        return $this->competitor;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->competitor = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add competitor
     *
     * @param \Alliance\CompetitionBundle\Entity\Competitor $competitor
     * @return WeightCategory
     */
    public function addCompetitor(\Alliance\CompetitionBundle\Entity\Competitor $competitor)
    {
        $this->competitor[] = $competitor;
    
        return $this;
    }

    /**
     * Remove competitor
     *
     * @param \Alliance\CompetitionBundle\Entity\Competitor $competitor
     */
    public function removeCompetitor(\Alliance\CompetitionBundle\Entity\Competitor $competitor)
    {
        $this->competitor->removeElement($competitor);
    }
}
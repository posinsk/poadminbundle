<?php

namespace Alliance\CompetitionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Users
 *
 * @ORM\Table("competition_user")
 * @ORM\Entity(repositoryClass="Alliance\CompetitionBundle\Entity\UsersRepository")
 */
class Users implements UserInterface, \Serializable, \Symfony\Component\Security\Core\User\EquatableInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotNull(message="Wypełnij to pole")
     * @ORM\Column(name="first_name", type="string", length=255)
     */
    private $firstName;

    /**
     * @var string
     * @Assert\NotNull(message="Wypełnij to pole")
     * @ORM\Column(name="last_name", type="string", length=255)
     */
    private $lastName;

    /**
     * @var string
     * @Assert\NotNull(message="Wypełnij to pole")
     * @ORM\Column(name="team", type="string", length=255)
     */
    private $team;

    /**
     * @var string
     * @Assert\Email(message="Wpis poprawny adres email")
     * @Assert\NotNull(message="Wypełnij to pole")
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    private $email;

    /**
     * @var string
     * @Assert\Length(min="6", minMessage="Hasło musi mieć przynajmniej 6 znaków")
     * @Assert\NotNull(message="Wypełnij to pole")
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity="Competitor", mappedBy="user")
     */
    private $competitors;

    public function __construct() {
        $this->competitors = new \Doctrine\Common\Collections\ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    public function getUsername() {
        return $this->getEmail();
    }

    public function getRoles() {
        return array('ROLE_USER');
    }

    public function eraseCredentials() {
        return true;
    }

    public function serialize() {
        return serialize($this->id);
    }

    public function unserialize($serialized) {
        $this->id = unserialize($serialized);
    }

    public function isEqualTo(UserInterface $user) {
        if ($user->getId() === $this->getId()) {
            return true;
        }
        return false;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    public function getSalt() {
        return null;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Users
     */
    public function setFirstName($firstName) {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName() {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Users
     */
    public function setLastName($lastName) {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName() {
        return $this->lastName;
    }

    /**
     * Set team
     *
     * @param string $team
     * @return Users
     */
    public function setTeam($team) {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return string 
     */
    public function getTeam() {
        return $this->team;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Users
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Users
     */
    public function setPassword($password) {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Users
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Add competitors
     *
     * @param \Alliance\CompetitionBundle\Entity\Competitor $competitors
     * @return Users
     */
    public function addCompetitor(\Alliance\CompetitionBundle\Entity\Competitor $competitors) {
        $this->competitors[] = $competitors;

        return $this;
    }

    /**
     * Remove competitors
     *
     * @param \Alliance\CompetitionBundle\Entity\Competitor $competitors
     */
    public function removeCompetitor(\Alliance\CompetitionBundle\Entity\Competitor $competitors) {
        $this->competitors->removeElement($competitors);
    }

    /**
     * Get competitors
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCompetitors() {
        return $this->competitors;
    }

}
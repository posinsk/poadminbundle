<?php

namespace PO\MeterBundle\Entity;

use Doctrine\ORM\EntityRepository;
use PO\MeterBundle\Entity\Measure;
/**
 * MeasureRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class MeasureRepository extends EntityRepository
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    public function __construct(\Doctrine\ORM\EntityManager $em, $class)
    {
        parent::__construct($em, $class);
        $this->em = $this->getEntityManager();
    }
    
    public function addMeasure($value, $type){
        if($value == null) return false;
        
        $today = new \DateTime();
        
        $measures = $this->createQueryBuilder('m')
                ->where('m.createdAt <= :today')
                ->setParameter('today', $today)
                ->andWhere('m.type = :type')
                ->setParameter('type', $type)
                ->orderBy('m.createdAt', 'DESC')
                ->getQuery()
                ->setMaxResults(2)
                ->getResult();
        
        //if measures are from today
        if(count($measures) == 2 && $measures[1]->getDate() == $today->format('d-m-Y')
                || (isset($measures[0]) && $measures[0]->getDate() == $today->format('d-m-Y')))
            return false;
             
        if(isset($measures[0]))
            $lastMeasure = $this->fillDateGaps($measures, $value);
        
        $measure = new Measure;
        $measure->setType($type);
        $measure->setState($value);
        $change = isset($measures[0]) ? $value - $lastMeasure->getState() : 0;
        
        $measure->setChange($change);
        
        $this->em->persist($measure);
        $this->em->flush();
        
        return true;
    }
    
    private function fillDateGaps($measures, $newValue){
        $lastMeasure = $measures[0];
        $today = new \DateTime;
        
        $dateInterval = $today->diff($lastMeasure->getCreatedAt());
        $days = $dateInterval->d;

        if($days > 1){
            $medianChange = round(($newValue - $lastMeasure->getState())/($days), 3);
            
            for($i=1;$i<=$days-1;$i++){
                $measure = new Measure;
                $measure->setType($lastMeasure->getType());
                $measure->setState($lastMeasure->getState() + ($medianChange * $i));
                $measure->setChange($medianChange);
                
                $measure->setCreatedAt($lastMeasure->getCreatedAt()->modify('+1 day'));
                
                $this->em->persist($measure);
                $this->em->flush();
            }
            return $measure;
        }
        return $lastMeasure;
    }
    
}

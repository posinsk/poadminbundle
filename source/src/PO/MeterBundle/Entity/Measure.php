<?php

namespace PO\MeterBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Measure
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="PO\MeterBundle\Entity\MeasureRepository")
 */
class Measure
{
    
    const GAS = 0;
    const ELECTRICITY = 1;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="state", type="float", nullable=false)
     */
    private $state;

    /**
     * @var float
     *
     * @ORM\Column(name="change", type="float", nullable=false)
     */
    private $change;
    
    /**
     * @ORM\Column(name="type", type="integer", nullable=false)
     */
    private $type;
    
    /**
     * @ORM\Column(name="created_at", type="datetime") 
     */
    private $createdAt;
    
    
    public function __construct(){
        $this->createdAt = new \DateTime;
    }
    
    public function getDate(){
        return $this->createdAt->format('d-m-Y');
    }
    
    public function getDayBeforeDate(){
        return $this->createdAt->modify('-1 day')->format('d-m-Y');
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set state
     *
     * @param float $state
     * @return Measure
     */
    public function setState($state)
    {
        $this->state = $state;
    
        return $this;
    }

    /**
     * Get state
     *
     * @return float 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set change
     *
     * @param float $change
     * @return Measure
     */
    public function setChange($change)
    {
        $this->change = $change;
    
        return $this;
    }

    /**
     * Get change
     *
     * @return float 
     */
    public function getChange()
    {
        return $this->change;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Measure
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Measure
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
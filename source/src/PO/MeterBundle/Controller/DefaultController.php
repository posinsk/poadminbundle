<?php

namespace PO\MeterBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Validator\Constraints as Assert;

class DefaultController extends Controller
{

    /**
     * @Route("/measure", name="meter_index")
     * @Template()
     */
    public function indexAction(\Symfony\Component\HttpFoundation\Request $r)
    {
        $form = $this->createFormBuilder()
                ->add('gas', 'integer', array(
                    'constraints' => new Assert\Range(array('min' => 0)),
                    'attr' => array('placeholder' => '123,123')
                ))
                ->add('electricity', 'integer', array('constraints' => new Assert\Range(array('min' => 0)),
                    'attr' => array('placeholder' => '123,123')
                ))
                ->getForm();


        if ($r->isMethod('POST')) {
            $form->bind($r);
            if ($form->isValid()) {
                $data = $form->getData();

                $repo = $this->getDoctrine()->getRepository('POMeterBundle:Measure');
                if ($repo->addMeasure($data['gas'], \PO\MeterBundle\Entity\Measure::GAS))
                    $this->get('session')->getFlashBag()->add('notice', 'Gas added!');

                if ($repo->addMeasure($data['electricity'], \PO\MeterBundle\Entity\Measure::ELECTRICITY))
                    $this->get('session')->getFlashBag()->add('notice', 'Electricity added!');

                return $this->redirect($this->generateUrl('meter_index'));
            }
        }

        return array('form' => $form->createView());
    }

}

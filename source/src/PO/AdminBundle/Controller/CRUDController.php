<?php

namespace PO\AdminBundle\Controller;

use PO\AdminBundle\Controller\MyController\MyController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @todo: data transformer, admin bar
 * 
 * @Route("/crud")
 */
class CRUDController extends MyController {
    
    private function invokeConfig($table){
        $config = $this->container->getParameter('admin');
        $this->adminNamespace = $config['namespace'];
        
        $className = $this->adminNamespace.$table;
        $p = new $className($this->getDoctrine()->getManager());

        return $p;
    }
    
    private function checkAccess($roles){
        $access = $this->get('po.access');
        return $access->checkAccess($roles);
    }
    
    /**
     * @Route("/{table}/list/{page}", name="po_admin_list", defaults={"page" = 1})
     * @Template()
     */
    public function listAction(Request $request, $table, $page) {
        $config = $this->invokeConfig($table);
        $list = $config->listing();

        if(!($list instanceof \PO\AdminBundle\AdminCore\AdminList))
            throw new \Exception('You must return an object of \PO\AdminBundle\AdminCore\AdminList in listing() method in file '.$table.'.php');
        
        //check if user have access to this section
        $admin = $this->container->getParameter('admin');
        if(array_key_exists('secure', $admin['menu'][$table]) 
                && !$this->checkAccess($admin['menu'][$table]['secure']))
                return $this->redirect($this->generateUrl('po_admin_dashboard'));
        
        //detect fields
        if(!count($list->getFields()))
            $fields = $this->recognizeFields($list->getQuery(), $config->entityNamespace);
        else
            $fields = $list->getFields();
        
        //detect filters
        extract($this->makeListFilters($list, $request));
        
        //handle searching if is initialized
        extract($this->handleSearchBy($list, $request));
        
        //set order by
        extract($this->setOrderBy($list, $request));

        //fetch results from query and build pagination
        if(!$list->isPaginationDisabled()){
            $paginator = $this->get('po.paginator');
            if($list->getPaginationPerPage())
                $paginator->setConfig($list->getPaginationPerPage());
            if($list->getPaginationPageRange()) 
                $paginator->setConfig(null, $list->getPaginationPageRange());

            $items = $paginator->buildPage($query, $page, $currentOrderByExpression);
            $paginationData = $paginator->getPaginationData();

            if($items == null && $page > 1) 
                return $this->redirect($this->generateUrl ('po_admin_list', array(
                    'table'=>$table, 
                    'page'=>$paginationData['last']
                    )));
        }else
            $items = $query->getQuery()->getResult();
            
        return compact('fields', 'items', 'paginationData', 'filters', 'config');
    }
    
    private function handleSearchBy($list, Request $request){
        $query = $list->getQuery();
        if($request->query->has('searchby') && $request->get('search_phrase')
                && strlen($request->get('search_phrase')) >= $list->getMinSearchByPhraseLength()){
            $searchBy = $list->getSearchBy();
            $rootE = $query->getRootAlias();
            $field = $searchBy[$request->get('searchby')]['field'];
            $query->andWhere('lower('.$rootE.'.'.$field.') LIKE :p')
               ->setParameter(':p', '%'.strtolower(trim($request->get('search_phrase'))).'%');
        }
        
        return compact('query');
        
    }
    
    private function setOrderBy($list, Request $request){
        $orderBy = $list->getOrderBy();
        $query = $list->getQuery();
        
        if($request->get('orderby')){
            $key = explode('-', $request->get('orderby'));
            $currentOrderByExpression = $orderBy[$key[0]]['expression'];
            $query->orderBy($orderBy[$key[0]]['expression'], $key[1]);
        }else{
            $currentOrderByExpression = false;
        }
        
        return compact('query', 'currentOrderByExpression');
    }

    private function makeListFilters($list, Request $request){
        $filters = $list->getFilters();
        $query = $list->getQuery();
        
        if($list->getFiltersWithCounter()){
            foreach($filters as $key => $filter){
                $clonedQ = clone $query;
                if($filter['condition'] != null)
                    $clonedQ->andWhere($filter['condition']);
                $count = $clonedQ->select('count(distinct '.$clonedQ->getRootAlias().')')
                        ->getQuery()->getSingleResult();
                $filters[$key]['count'] = $count[1];
            }
        }
        
        if($request->get('filter') && 
            array_key_exists($request->get('filter'), $filters)){
            $query->andWhere($filters[$request->get('filter')]['condition']);
        }   
        
        return compact('filters', 'query');
    }

    /**
     * @Route("/{table}/delete/{id}", name="po_admin_delete")
     */
    public function deleteAction(Request $request, $table, $id){
        $config = $this->invokeConfig($table);
        $d = $config->delete();
        
        if(!($d instanceof \PO\AdminBundle\AdminCore\CRUD\Delete))
            throw new \Exception('delete() method should return an object of \PO\AdminBundle\AdminCore\CRUD\Delete');
        
        if(!$this->checkAccess($d->getAccessRoles()))
            return $this->redirect($this->generateUrl('po_admin_dashboard'));
        
        if($d->getRoute())
                return $this->redirect($this->generateUrl($d->getRoute()));

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository($config->entityNamespace)
                ->find($id);
        
        $entity = $d->invokePreFlush($entity);
        
        if($d->getField())
            $entity->{$d->getFieldMethod()}(true);
        else
            $em->remove($entity);
        
        $em->flush();
        
        $this->getLogger()->setSubject($table, $id)->logRemove();
        
        $eventDispatcher = $this->getEventDispatcher();
        $eventDispatcher->dispatch('po_admin.crud.delete', new \PO\AdminBundle\Event\CRUDDelete($table, $entity));
        
        $referer = $request->headers->get('referer');       
 
        return new RedirectResponse($referer);
    }
    
    /**
     * @Route("/{table}/create", name="po_admin_create")
     * @Template("POAdminBundle:CRUD:createUpdate.html.twig")
     */
    public function createAction(Request $request, $table){
        $config = $this->invokeConfig($table);
        $c = $config->create();

        if(!($c instanceof \PO\AdminBundle\AdminCore\CRUD\Create))
            throw new \Exception('create() method should return an object of \PO\AdminBundle\AdminCore\CRUD\Create');
        
        if(!$this->checkAccess($c->getAccessRoles()))
            return $this->redirect($this->generateUrl('po_admin_dashboard'));
        
        if($c->getRoute())
                return $this->redirect($this->generateUrl($c->getRoute()));

        $entity = new $config->entityNamespace;
        
        $typeName = $c->getType();
        $form = $this->createForm(new $typeName, $entity);
        
        if('POST' == $request->getMethod()){
            $form->bind($request);
            
            $form = $c->invokePreValid($form);
            
            if ($form->isValid()) {

                $entity = $c->invokePreFlush($entity);
                
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                
                $id = method_exists($entity, 'getId') ? $entity->getId() : null;
                $this->getLogger()->setSubject($table, $id)->logCreate();
                
                $this->get('session')->setFlash('notification', 'Item created');
                return $this->redirect($this->generateUrl('po_admin_list', array(
                    'table'=>$table
                )));
            }
        }
        
        return array(
            'form' => $form->createView()
        );
        
    }
    
    
    /**
     * @Route("/{table}/update/{id}", name="po_admin_update")
     * @Template("POAdminBundle:CRUD:createUpdate.html.twig")
     */
    
    public function updateAction(Request $request, $table, $id){
        $config = $this->invokeConfig($table);
        $u = $config->update();
        
        if(!($u instanceof \PO\AdminBundle\AdminCore\CRUD\Update))
            throw new \Exception('update() method should return an object of \PO\AdminBundle\AdminCore\CRUD\Update');
        
        if(!$this->checkAccess($u->getAccessRoles()))
            return $this->redirect($this->generateUrl('po_admin_dashboard'));
        
        if($u->getRoute())
                return $this->redirect($this->generateUrl($u->getRoute(), array('id'=>$id)));
        
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository($config->entityNamespace)->find($id);
        
        $typeName = $u->getType();
        $form = $this->createForm(new $typeName, $entity);
        
        if('POST' == $request->getMethod()){
            $form->bind($request);
            
            $form = $u->invokePreValid($form);
            
            if ($form->isValid()) {
                
                $entity = $u->invokePreFlush($entity);
                
                $em->persist($entity);
                $em->flush();
                
                $this->getLogger()->setSubject($table, $id)->logUpdate();
                
                $this->get('session')->setFlash('notification', 'Item updated!');
                return $this->redirect($this->generateUrl('po_admin_update', array(
                    'table' => $table,
                    'id' => $entity->getId()
                )));
            }
        }
        
        return array(
            'form' => $form->createView(),
        );
    }
    
    
    /**
     * @Route("/{table}/read/{id}", name="po_admin_read")
     * @Template()
     */
    public function readAction($table, $id){
        $config = $this->invokeConfig($table);
        $read = $config->read();
        
        if(!($read instanceof \PO\AdminBundle\AdminCore\CRUD\Read))
            throw new \Exception('read() method should return an object of \PO\AdminBundle\AdminCore\CRUD\Read');
        
        if(!$this->checkAccess($read->getAccessRoles()))
            return $this->redirect($this->generateUrl('po_admin_dashboard'));
        
        if($read->getRoute())
                return $this->redirect($this->generateUrl($read->getRoute(), array('id'=>$id)));
        
        $entity = $this->getDoctrine()->getRepository($config->entityNamespace)
                ->find($id);
        
        if($read->getAuto()){
            $f = $this->getDoctrine()
                        ->getManager()
                        ->getClassMetadata($config->entityNamespace)
                        ->fieldNames;
            $fields = array();
            foreach($f as $one)
                $fields[] = array('label'=>$one, 'field'=>$one);
        }else
            $fields = $read->getManualFields();
        
        $entity = $read->invokeOnRead($entity);
        
        $update = $config->update();

        return compact('entity', 'fields', 'read', 'update');
    }
    
    
    /**
     * @Route("/action/{table}/{route}", name="po_admin_custom_action")
     */
    public function customAction(Request $request, $table, $route) {
        $params = $request->query;
        $paramsArray = iterator_to_array($params->getIterator());
        
        $this->getLogger()->setSubject($route)->logCustomAction($table);
        
        return $this->redirect($this->generateUrl($route, $paramsArray));
    }
    
    

    private function recognizeFields(\Doctrine\ORM\QueryBuilder $query, $entity) {
        $ra = $query->getRootAlias();
        foreach ($query->getQuery()->getAST()->selectClause->selectExpressions as $clause) {
            if (is_object($clause->expression)) {
                if ($clause->expression->identificationVariable == $ra) {
                    // select only listed fields
                    $fields = $clause->expression->partialFieldSet;
                }
            } elseif ($clause->expression == $ra) {
                $fields = $this->getDoctrine()
                        ->getManager()
                        ->getClassMetadata($entity)
                        ->fieldNames;
            }
        }
        
        $newArr = array();
        foreach($fields as $field)
            $newArr[] = array('label'=>$field, 'field'=>$field);
        
        return $newArr;
    }

}
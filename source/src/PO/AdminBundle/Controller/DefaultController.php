<?php

namespace PO\AdminBundle\Controller;

use PO\AdminBundle\Controller\MyController\MyController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Finder\Finder;

/**
 * @Route("/")
 */
class DefaultController extends MyController
{

    /**
     * @Route("/login", name="po_admin_login")
     * @Template()
     */
    public function loginAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                    SecurityContext::AUTHENTICATION_ERROR
            );
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return array(
            'last_username' => $session->get(SecurityContext::LAST_USERNAME),
            'error' => $error,
        );
    }

    /**
     * @Route("/logout", name="po_admin_logout")
     * @Template()
     */
    public function logoutAction()
    {
        return array();
    }

    /**
     * @Route("/", name="po_admin_dashboard")
     * @Template()
     */
    public function dashboardAction(Request $r)
    {
        $config = $this->container->getParameter('admin');
        $dashboard = $config['dashboard'];
        $dashboard = new $dashboard();
        return compact('dashboard');
    }

    /**
     * @Route("/clear-cache", name="po_admin_cc")
     */
    public function clearCacheAction(Request $r)
    {
        apc_clear_cache();
        apc_clear_cache('user');

        $referer = $r->headers->get('referer');
        if ($referer)
            return new RedirectResponse($referer);
        else
            return $this->redirect($this->generateUrl('po_admin_dashboard'));
    }

    /**
     * @Route("/read-log", name="po_admin_log")
     * @Template
     */
    public function logAction(Request $r)
    {
        $access = $this->get('po.access');
        $config = $this->container->getParameter('admin');

        if (!array_key_exists('logging', $config)
                && !array_key_exists('secure', $config['logging'])
                || !$access->checkAccess($config['logging']['secure']))
            return $this->redirect($this->generateUrl('po_admin_dashboard'));

        $logsDir = $this->container->get('kernel')->getRootDir() . '/logs';
        
        if($r->get('code')){
            $logFile = $logsDir.'/'.base64_decode($r->get('code'));
            $content = \PO\AdminBundle\Service\CmfLogger\MyLogger::parseLog($logFile);
        }else
            $files = $this->findLogFiles($logsDir);
        
        return compact('files', 'content');
    }

    private function findLogFiles($logsDir) {

        $finder = new Finder();
        $finder->files()->in($logsDir)->name('poadminlog*.log')->sortByModifiedTime();
        $files = array();
        foreach ($finder as $logFile) {
            $files[] = array(
                'filename' => $logFile->getFilename(),
                'code' => base64_encode($logFile->getFilename())
            );
        }
        
        return array_reverse($files);
    }
    
    /**
     * @Route("/_proxy/menu", name="po_admin_menu")
     * @Template("POAdminBundle:Default:_menu.html.twig")
     */
    public function menuAction(Request $r)
    {
        $config = $this->container->getParameter('admin');
        $this->adminNamespace = $config['namespace'];
        $access = $this->get('po.access');

        $pattern = "/Controller\\\\([a-zA-Z]*)Controller/";
        $matches = array();
        preg_match($pattern, $r->get("controller"), $matches);

        $cache = $this->get('po.cache');
        $menu = $cache->get('menuList');
        if (!$menu) {
            $menu = array();
            foreach ($config['menu'] as $key => $position) {

                //check if user has permission to see this position
                if (array_key_exists('secure', $position)) {
                    $allowed = $access->checkAccess($position['secure']);
                    if (!$allowed)
                        continue;
                }

                $pos = new \stdClass();
                $pos->key = $key;
                $pos->label = $position['label'];
                $pos->icon = $position['icon'];

                if (method_exists($this->adminNamespace . ucfirst($key), 'create')) {
                    $obj = $this->adminNamespace . $key;
                    $instance = new $obj($this->getDoctrine()->getManager());
                    $create = $instance->create();
                    if ($access->checkAccess($create->getAccessRoles())) {
                        $pos->create = true;
                    }else
                        $pos->create = false;
                }else
                    $pos->create = false;

                if (method_exists($this->adminNamespace . ucfirst($key), 'getSubLabel')) {
                    $class = $this->adminNamespace . ucfirst($key);
                    $instance = new $class($this->getDoctrine()->getManager());
                    $pos->subLabel = $instance->getSubLabel();
                }

                $menu[] = $pos;
            }
            $cache->set('menuList', $menu, 60);
        }

        if (array_key_exists('secure', $config['upload'])) {
            $uploadAllowed = $access->checkAccess($config['upload']['secure']) ?
                    true : false;
        } else {
            $uploadAllowed = true;
        }

        return array(
            'controller' => $matches[1],
            'menu' => $menu,
            'upload' => $uploadAllowed
        );
    }

}
<?php
namespace PO\AdminBundle\Controller\MyController;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MyController extends Controller
{
    public function getLogger(){
        return $this->get('po.logger');
    }
    
    public function getEventDispatcher(){
        return $this->container->get('event_dispatcher');
    }
}

?>

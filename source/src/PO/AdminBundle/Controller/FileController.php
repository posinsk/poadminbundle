<?php

namespace PO\AdminBundle\Controller;

use PO\AdminBundle\Controller\MyController\MyController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Route("/files")
 */
class FileController extends MyController {

    /**
     * @Route("/list", name="po_admin_file_list")
     * @Template()
     */
    public function listAction(Request $r) {
        $config = $this->getConfig();
        $access = $this->get('po.access');
        
        if(array_key_exists('secure', $config) &&
                !$access->checkAccess($config['secure']))
            return $this->redirect($this->generateUrl('po_admin_dashboard'));
        
        $files = array();
        $dirs = array();
        $currentPath = $r->get('path');

        $dir = $config['webdir'] . $currentPath;

        $fileFinder = new Finder();
        $fileFinder->files()->in($dir)->depth('== 0');

        $dirFinder = new Finder();
        $dirFinder->directories()->in($dir)->depth('== 0');

        //sorting
        $sort = $r->get('sort', 'name-DESC');
        $sort = explode('-', $sort);
        switch ($sort[0]) {
            default:
            case 'name':
                $fileFinder->sortByName();
                $dirFinder->sortByName();
                break;
            case 'time':
                $fileFinder->sortByModifiedTime();
                $dirFinder->sortByModifiedTime();
                break;
        }

        foreach ($fileFinder as $file) {
            $filename = $file->getFilename();
            $files[] = array(
                'name' => $filename,
                'size' => filesize($dir . '/' . $filename),
                'mtime' => filemtime($dir . '/' . $filename)
            );
        }

        foreach ($dirFinder as $dir) {
            $dirs[] = array(
                'name' => $dir->getFilename(),
                'mtime' => filectime($dir . '/')
            );
        }

        if ($sort[1] == 'ASC')
            $files = array_reverse($files);
        else
            $dirs = array_reverse($dirs);


        $uploadForm = $this->getUploadForm($currentPath);
        $newdirForm = $this->getCreateDirForm($currentPath);

        return array(
            'files' => $files,
            'dirs' => $dirs,
            'uploadForm' => $uploadForm->createView(),
            'newdirForm' => $newdirForm->createView(),
            'freeSpace' => disk_free_space('/'),
            'config' => $config
        );
    }

    /**
     * @Route("/delete", name="po_admin_file_delete")
     */
    public function deleteAction(Request $r) {
        $config = $this->getConfig();
        $webDir = $config['webdir'];
        $dirs = $r->get('dir', array());
        $files = $r->get('file', array());

        if(!is_array($dirs)) $dirs = array($dirs);        
        foreach($dirs as $dir){
            $this->getLogger()->setSubject($dir)->logDirRemove();
            $this->rmdirRecursive($webDir . '/' . substr($dir, 1));
        }

        if(!is_array($files)) $files = array($files);        
        foreach($files as $file){
            $this->getLogger()->setSubject($file)->logFileRemove();
            unlink($webDir . '/' . substr($file, 1));
        }

        $referer = $r->headers->get('referer');
        return new RedirectResponse($referer);
    }

    public function rmdirRecursive($dir) {
        foreach (glob($dir . '/*') as $file) {
            if (is_dir($file))
                $this->rmdirRecursive($file);
            else
                unlink($file);
        }
        rmdir($dir);
    }

    /**
     * @Route("/upload", name="po_admin_file_upload")
     */
    public function uploadAction(Request $request) {
        $config = $this->getConfig();
        $form = $this->getUploadForm();

        if ($request->isMethod('POST')) {
            $form->bind($request);
            if ($form->isValid()) {
                $file = $form['file']->getData();
                $uploadedFilename = $file->getClientOriginalName();
                
                $path = $request->get('path') ? $request->get('path') : null;
                $webUpload = $config['webdir'] . '/' . $path;

                //look for a file with the same name and extension in upload dir
                $finder = new Finder();
                $finder->files()->in($webUpload);
                foreach ($finder as $fileI) {
                    //if file found
                    if ($fileI->getFilename() == $uploadedFilename) {
                        $filename = explode('.', $uploadedFilename);
                        //set new name
                        $extension = $file->guessExtension();
                        if (!$extension)
                            $extension = 'bin';

                        $uploadedFilename = $filename[0] . substr(uniqid(), 10, 3) . '.' . $extension;
                    }
                }
                $file->move($config['dir'] . $form->get('path')->getData(), $uploadedFilename);
                $this->getLogger()->setSubject($form->get('path')->getData().'/'.$uploadedFilename)->logFileUpload();
            }else
                $this->get('session')->setFlash('notification', 'An error occured while uploading a file/creating a dir');
        }

        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);
    }

    /**
     * @Route("/details", name="po_admin_file_details")
     * @Template
     */
    public function detailsAction(Request $request) {
        if(!$request->query->has('path'))
            return $this->redirect($this->generateUrl('po_admin_file_list'));

        $config = $this->getConfig();
        $f = explode('/', $request->get('path'));
        
        $file = explode('.', array_pop($f));
        $name = array('name'=>$file[0]);
        $form = $this->createFormBuilder($name)
                ->add('name', 'text')
                ->getForm();

        if ($request->isMethod('POST')) {
            $form->bind($request);
            if ($form->isValid()) {
                $name = $form->get('name')->getData();
                $oldName = $request->get('path');
                $extension = count($file) > 1 ? '.'.$file[1] : null;
                $newName = implode('/', $f).'/'.$name.$extension;
                rename($config['webdir'].$oldName, $config['webdir'].$newName);
                $this->getLogger()->setSubject($oldName.' to '.$newName)->logFilenameChange();
                $this->get('session')->setFlash('notification', 'Item name changed!');
            }
        }

        return array('form' => $form->createView());
    }

    /**
     * @Route("/newdir", name="po_admin_file_newdir")
     */
    public function newDirAction(Request $request) {
        $config = $this->getConfig();
        $form = $this->getCreateDirForm();

        if ($request->isMethod('POST')) {
            $form->bind($request);
            if ($form->isValid()) {
                $path = $form->get('path')->getData();
                $dirName = $form->get('dir_name')->getData();
                mkdir($config['webdir'] . '/' . $path . '/' . $dirName);
                $this->getLogger()->setSubject($path . '/' . $dirName)->logDirCreate();
            }
        }

        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);
    }

    private function getUploadForm($path = null) {
        $config = $this->getConfig();
        $mimeTypes = array_key_exists('allowed_mime_types', $config) ? $config['allowed_mime_types'] : false;
        $maxSize = array_key_exists('file_max_size', $config) ? $config['file_max_size'] : false;
        
        $fileConstraints = array();
        
        if($mimeTypes) $fileConstraints['mimeTypes'] = $mimeTypes;
        if($maxSize) $fileConstraints['maxSize'] = $maxSize;

        $form = $this->createFormBuilder(array('path' => $path), array(
                    'cascade_validation' => true
                ))
                ->add('file', 'file', array(
                    'constraints' => array(
                        new Assert\NotBlank(),
                        new Assert\File($fileConstraints)
                        )))
                ->add('path', 'hidden', array(
                    'constraints' => array(
                        new Assert\Length(array('min' => 1)),
                        )))
                ->getForm();
        return $form;
    }

    private function getCreateDirForm($path = null) {
        $form = $this->createFormBuilder(array('path' => $path), array(
                    'cascade_validation' => true
                ))
                ->add('dir_name', 'text', array(
                    'constraints' => array(
                        new Assert\NotBlank(),
                        )))
                ->add('path', 'hidden', array(
                    'constraints' => array(
                        new Assert\Length(array('min' => 1)),
                        )))
                ->getForm();
        return $form;
    }

    private function getConfig() {
        $adminConfig = $this->container->getParameter('admin');
        $config = $adminConfig['upload'];
        $webDir = array_key_exists('webdir', $config) ? '/' . $config['webdir'] : null;
        $config['webdir'] = $this->get('kernel')->getRootDir() . $webDir . '/' . $config['dir'];
        return $config;
    }

}
<?php

namespace PO\AdminBundle\EventListener;

use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use PO\AdminBundle\Service\CmfLogger\MyLogger;

class LoginListener
{

    private $logger;

    public function __construct(MyLogger $logger)
    {
        $this->logger = $logger;
    }

    public function onLogin(InteractiveLoginEvent $event)
    {
        $this->logger->logUserLogin();
    }

}
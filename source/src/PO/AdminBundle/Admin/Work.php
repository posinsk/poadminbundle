<?php

namespace PO\AdminBundle\Admin;

use PO\AdminBundle\AdminCore\PositionCore;
use PO\AdminBundle\AdminCore\AdminList;
use PO\AdminBundle\AdminCore\CustomAction;
use PO\AdminBundle\AdminCore\CRUD\Create;
use PO\AdminBundle\AdminCore\CRUD\Update;
use PO\AdminBundle\AdminCore\CRUD\Read;
use PO\AdminBundle\AdminCore\CRUD\Delete;

class Work extends PositionCore {

    public $label = 'Prace';
    public $entityNamespace = 'PiotrOsinski\Bundle\Entity\Work';

    public function listing() {

        $query = $this->getEntityManager()
                ->getRepository('PiotrOsinskiBundle:Work')
                ->createQueryBuilder('p');

        $list = new AdminList($query);

        $list->disablePagination();

//        $list->addField('Id', 'id');
        $list->addField('Tytuł', 'Name');
//        $list->addField('Kategorie', 'categoriesString');
//        $list->addField('Status', 'type', array(true));
//        $list->addField('Stworzono', 'createdAt');
//        $list->addField('Ostatnia edycja', 'updatedAt');

        return $list;
    }

    public function create() {
        $em = $this->getEntityManager();
        $create = Create::initialize()->setType('PiotrOsinski\Bundle\Form\Admin\WorkType')
                ->preFlush(function($entity) use($em) {
                    return \PO\AdminBundle\Admin\Work::persistEntity($entity, $em);
                });
        return $create;
    }

    public function update() {
        $em = $this->getEntityManager();
        $update = Update::initialize()->setType('PiotrOsinski\Bundle\Form\Admin\WorkType')
                ->preFlush(function($entity) use($em) {
                    return \PO\AdminBundle\Admin\Work::persistEntity($entity, $em);
                });
        ;
        return $update;
    }

    public function read() {
        $read = Read::initialize()
                ->addManualField('Tytuł', 'name')
                ->addManualField('Obrazki', 'media.absoluteWebPathWithTag')
                ->setRawFields(array('Obrazki'));
        return $read;
    }

    public function delete() {
        $delete = Delete::initialize()->setAuto()
                ->preFlush(function ($entity) {
                    foreach ($entity->getMedia() as $media) {
                        $this->getEntityManager()->remove($media);
                    }
                    $this->getEntityManager()->flush();
                    return $entity;
                });
        return $delete;
    }

    public static function persistEntity(\PiotrOsinski\Bundle\Entity\Work $entity, $em) {
        foreach ($entity->getMedia() as $media) {
            if ($media->getFile())
                $em->persist($media);
            else {
                $entity->removeMedia($media);
            }
        }
        return $entity;
    }

}

?>

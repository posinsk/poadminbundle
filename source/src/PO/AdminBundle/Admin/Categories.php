<?php

namespace PO\AdminBundle\Admin;

use PO\AdminBundle\AdminCore\PositionCore;
use PO\AdminBundle\AdminCore\AdminList;
use PO\AdminBundle\AdminCore\CustomAction;

class Categories extends PositionCore{
    
    public $label = 'Kategorie';
    public $entity = 'PiotrOsinski\Bundle\Entity\Blog\Category';
    
    public function listing(){
        $em = $this->getEntityManager();
        
        $q = $em->getRepository('PiotrOsinskiBundle:Blog\Category')
                ->createQueryBuilder('c')
                ->select('c');
        
        $list = new AdminList($q);

        $list->disablePagination();
        
        $list->addSearchBy('Nazwa kategorii', 'name');
        
        $list->addField('Id', 'id');
        $list->addField('Nazwa', 'name');
//        $list->addField('Stworzono', 'createdAt');

        $list->addOrderBy('Tytuł', 'c.name');
        $list->addOrderBy('Data utworzenia', 'c.createdAt');
        
        
        return $list;
    }
    
}

?>

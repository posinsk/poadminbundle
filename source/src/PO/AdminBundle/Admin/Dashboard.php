<?php

namespace PO\AdminBundle\Admin;

use PO\AdminBundle\AdminCore\AdminDashboard;
use PO\AdminBundle\AdminCore\Dashboard\Template;
use PO\AdminBundle\AdminCore\Dashboard\Controller;
use PO\AdminBundle\AdminCore\Dashboard\Scaffold;

class Dashboard extends AdminDashboard{
    
    public function __construct(){
        $templ1 = new Template('tytul', Scaffold::BOX_LARGE);
        $templ1->setTemplate('PiotrOsinskiBundle:Blog\Default:empty.html.twig', array(
            'argument1'=>10,
            'argument2'=>20
            ));
        $this->addTemplate($templ1);
        
        $templ2 = new Template('tytul2 small', Scaffold::BOX_SMALL);
        $templ2->setTemplate('PiotrOsinskiBundle:Blog\Default:empty.html.twig', array(
            'argument1'=>1,
            'argument2'=>2
            ));
        $this->addTemplate($templ2);
        
        $controller = new Controller('kontroler', Scaffold::ROW);
        $controller->setController('PiotrOsinskiBundle:Blog\Default:empty', array(
            'argument1'=>34, 
            'argument2'=>33
            ));
        $this->addController($controller);
        
        $this->setTitle('Dashhhh');
    }
    
}

?>

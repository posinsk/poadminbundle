<?php

namespace PO\AdminBundle\Admin;

use PO\AdminBundle\AdminCore\PositionCore;

use PO\AdminBundle\AdminCore\AdminList;
use PO\AdminBundle\AdminCore\CRUD\Create;
use PO\AdminBundle\AdminCore\CRUD\Update;
use PO\AdminBundle\AdminCore\CRUD\Read;
use PO\AdminBundle\AdminCore\CRUD\Delete;


class Media extends PositionCore{
    
    public $label = 'Prace';
    public $entityNamespace = 'PiotrOsinski\Bundle\Entity\Media';
    
    public function listing(){

        $query = $this->getEntityManager()
                ->getRepository('PiotrOsinskiBundle:Media')
                ->createQueryBuilder('p')
                ->select('p,w')
                ->leftJoin('p.work', 'w');
        
        $list = new AdminList($query);
        
        $list->setPaginationPerPage(20);
        
        $list->addField('Nazwa pliku', 'name');
        $list->addField('Praca id', 'work.id');
        $list->addField('Praca nazwa', 'work.name');
        
        $works = $this->getEntityManager()
                ->getRepository('PiotrOsinskiBundle:Work')
                ->findAll();

        foreach($works as $work)
            $list->addFilter ($work->getName(), 'w.id = '.$work->getId());
        
        return $list;
    }
    
    public function create(){
        $create = Create::initialize()->setType('PiotrOsinski\Bundle\Form\Admin\MediaWorkType');
        return $create;
    }

    public function update(){
        $update = Update::initialize()->setType('PiotrOsinski\Bundle\Form\Admin\MediaWorkType');
        return $update;
    }
    
    public function read(){
        $read = Read::initialize()->setAuto();
        return $read;
    }
    
    public function delete(){
        $delete = Delete::initialize()->setAuto();
        return $delete;
    }
    
}

?>

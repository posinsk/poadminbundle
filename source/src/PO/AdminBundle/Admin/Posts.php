<?php

namespace PO\AdminBundle\Admin;

use PO\AdminBundle\AdminCore\PositionCore;

use PO\AdminBundle\AdminCore\AdminList;
use PO\AdminBundle\AdminCore\CustomAction;
use PO\AdminBundle\AdminCore\CRUD\Create;
use PO\AdminBundle\AdminCore\CRUD\Update;
use PO\AdminBundle\AdminCore\CRUD\Read;
use PO\AdminBundle\AdminCore\CRUD\Delete;


class Posts extends PositionCore{
    
    public $label = 'Wpisy';
    public $entityNamespace = 'PiotrOsinski\Bundle\Entity\Blog\Post';
    
    public function listing(){

        $query = $this->getEntityManager()
                ->getRepository('PiotrOsinskiBundle:Blog\Post')
                ->createQueryBuilder('p')
                ->select('partial p.{id,title,type,createdAt,updatedAt}, c')
                ->leftJoin('p.categories', 'c');
        
        $list = new AdminList($query);
        $list->setPaginationPageRange(5);
        $list->setPaginationPerPage(5);
        
//        $list->disablePagination();
        
        $list->setFiltersWithCounter(true);
        
        $list->addSearchBy('Tytuł', 'title');
        
        $list->addField('Id', 'id');
        $list->addField('Tytuł', 'title');
        $list->addField('Kategorie', 'categoriesString');
        $list->addField('Status', 'type', array(true));
        $list->addField('Stworzono', 'createdAt');
        $list->addField('Ostatnia edycja', 'updatedAt');

        $list->addOrderBy('Tytuł', 'p.title');
        $list->addOrderBy('Data utworzenia', 'p.createdAt');
        $list->addOrderBy('Data ostatniej modyfikacji', 'p.updatedAt');
        
        $list->addFilter('Wszystkie', null);
        $list->addFilter('Szkic', 'p.type = 0');
        $list->addFilter('Opublikowany', 'p.type = 1');
        $list->addFilter('Ukryty', 'p.type = 2');
        $list->addFilter('Dostępny przez link', 'p.type = 3');
        
        return $list;
    }
    
    public function getSubLabel(){
        $c = $this->getEntityManager()
                ->getRepository('PiotrOsinskiBundle:Blog\Post')
                ->createQueryBuilder('p')
                ->select('count(p)')
                ->getQuery()->getResult();
        
        return $c[0][1];
//        return false;
    }
    
    public function customActions($entity){
        $action1 = new CustomAction('Akcja 1', 'admin_category', array(
            'my_param'=>123, 
            'sdfsd'=>$entity->getTitle()
                ));
        
        $action2 = new CustomAction('Akcja 2', 'admin_category', array(
            'my_param'=>123, 
            'sdfsd'=>$entity->getTitle()
                ));
        
//        $action1->setAccess(array('ROLE_USER'));
        
        $this->addCustomAction($action1);
        $this->addCustomAction($action2);
    }
    
    public function create(){
        $create = Create::initialize()
                ->setType('PiotrOsinski\Bundle\Form\Admin\PostType')
                ->preValid(function(\Symfony\Component\Form\Form $form){
                    return $form;
                })->preFlush(function($entity){
                    return $entity;
                });
        return $create;
    }

    public function update(){
        $update = Update::initialize()->setType('PiotrOsinski\Bundle\Form\Admin\PostType');
        return $update;
    }
    
    public function read(){
//        $read = Read::initialize()
//                ->addManualField('Stworzono', 'createdAt')
//                ->addManualField('Tytuł', 'title')
//                  ->setRawFields(array('content'));
        
        $read = Read::initialize()
                ->setAuto()
//                ->setAccess(array('ROLE_USER'))
                ->setRawFields(array('content'));
        return $read;
    }
    
    public function delete(){
        $delete = Delete::initialize()
//                ->setAccess(array('ROLE_USER'))
                ->setAuto();
        
        //SOFT DELETE
        //the field that will be set to true on delete, without deleting row
//        $delete->setField('isDeleted');
        return $delete;
    }
}

?>

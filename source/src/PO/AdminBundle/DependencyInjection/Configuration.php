<?php

namespace PO\AdminBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface {

    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder() {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('po_admin');

        $rootNode->children()
                ->scalarNode('namespace')->isRequired()->end()
                ->scalarNode('dashboard')->isRequired()->end()
                ->arrayNode('logging')
                    ->children()
                        ->arrayNode('secure')
                            ->prototype('scalar')->end()
                                ->children()->end()
                        ->end()
                        ->booleanNode('enabled')->defaultTrue()->end()
                        ->end()
                    ->end()
                ->arrayNode('upload')
                    ->children()
                        ->arrayNode('secure')
                            ->prototype('scalar')->end()
                                ->children()->end()
                        ->end()
                        ->scalarNode('webdir')->isRequired()->end()
                        ->scalarNode('dir')->isRequired()->end()
                        ->scalarNode('max_file_count_in_dir')->end()
                        ->scalarNode('file_max_size')->end()
                        ->arrayNode('allowed_mime_types')
                            ->prototype('scalar')->end()
                                ->children()->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('menu')
                    ->children()->end()
                        ->prototype('array')
                            ->children()
                                ->scalarNode('label')->end()
                                ->scalarNode('icon')->end()
                                ->arrayNode('secure')
                                    ->children()->end()
                                        ->prototype('scalar')->end()
                                            ->children()->end()
                                ->end()
                            ->end()
                     ->end()
                ->end();

        return $treeBuilder;
    }

    public function load(array $configs, ContainerBuilder $container) {
        $configuration = new Configuration();

        $config = $this->processConfiguration($configuration, $configs);

    }

}

<?php

namespace PO\AdminBundle\AdminCore;

abstract class AdminDashboard {

    private $boards = array();
    private $title = null;

    abstract function __construct();

    final public function addTemplate(Dashboard\Template $template) {
        $this->boards[] = $template;
        return $this;
    }

    final public function addController(Dashboard\Controller $controller) {
        $this->boards[] = $controller;
        return $this;
    }

    public function getBoards() {
        return $this->boards;
    }
    
    public function setTitle($title){
        $this->title = $title;
    }
    
    public function getTitle(){
        return $this->title;
    }

}

?>

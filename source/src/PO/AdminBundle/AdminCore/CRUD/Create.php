<?php

namespace PO\AdminBundle\AdminCore\CRUD;

use PO\AdminBundle\AdminCore\CRUD\CRUDInterface;

class Create extends CRUD implements CRUDInterface{

    private $route;
    private $type;
    
    public static function initialize(){
        return new \PO\AdminBundle\AdminCore\CRUD\Create();
    }
    
    public function setType($typeNamespace) {
        $this->clearConfig();
        $this->type = $typeNamespace;
        return $this;
    }

    public function setRoute($routeName) {
        $this->clearConfig();
        $this->route = $routeName;
        return $this;
    }
    
    public function getType(){
        return $this->type;
    }
    
    public function getRoute(){
        return $this->route;
    }

    private function clearConfig() {
        $this->route = null;
        $this->type = null;
    }
    
}
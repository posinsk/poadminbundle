<?php

namespace PO\AdminBundle\AdminCore\CRUD;

class CRUD {

    private $preValid = null;
    private $preFlush = null;
    private $onRead = null;
    private $accessRoles = array();
    
    public function setAccess(array $roles){
        $this->accessRoles = $roles;
        return $this;
    }
    
    public function getAccessRoles(){
        return $this->accessRoles;
    }

    public function preValid($function) {
        $this->preValid = $function;
        return $this;
    }
    
    public function preFlush($function) {
        $this->preFlush = $function;
        return $this;
    }
    
    public function onRead(\Closure $function) {
        $this->onRead = $function;
        return $this;
    }

    public function invokePreValid($form) {
        $f = $this->preValid;
        
        if(!$f) return $form;
        
        return $f($form);
    }

    public function invokePreFlush($entity) {
        $f = $this->preFlush;
        
        if(!$f) return $entity;
        
        return $f($entity);
    }

    public function invokeOnRead($entity) {
        $f = $this->onRead;
        
        if(!$f) return $entity;
        
        return $f($entity);
    }
}
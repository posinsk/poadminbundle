<?php

namespace PO\AdminBundle\AdminCore\CRUD;

class Read extends CRUD {

    private $route;
    private $auto;
    private $manual;
    private $rawFields;
    
    public static function initialize() {
        return new \PO\AdminBundle\AdminCore\CRUD\Read();
    }

    public function setAuto() {
        $this->clearConfig();
        $this->auto = true;
        return $this;
    }
    
    public function setRawFields($rawFields){
        $this->rawFields = $rawFields;
        return $this;
    }
    
    public function getRawFields(){
        return $this->rawFields;
    }

    public function setRoute($routeName) {
        $this->clearConfig();
        $this->route = $routeName;
        return $this;
    }
    
    public function addManualField($label, $field) {
        $this->clearConfig(array('route','auto'));
        $this->manual[] = compact('label', 'field');
        return $this;
    }

    public function getAuto() {
        return $this->auto;
    }

    public function getRoute() {
        return $this->route;
    }

    public function getManualFields() {
        return $this->manual;
    }

    private function clearConfig($fieldsToClear = true) {
        if($fieldsToClear === true){
            $this->route = null;
            $this->auto = null;
            $this->manual = null;
        }else{
            foreach($fieldsToClear as $field){
                $this->{$field} = null;
            }
        }
    }

}
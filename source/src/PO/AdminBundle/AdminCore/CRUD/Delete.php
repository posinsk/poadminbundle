<?php

namespace PO\AdminBundle\AdminCore\CRUD;

class Delete extends CRUD{
    
    private $route;
    private $auto;
    private $field;
    
    public static function initialize() {
        return new \PO\AdminBundle\AdminCore\CRUD\Delete();
    }

    public function setAuto() {
        $this->clearConfig();
        $this->type = true;
        return $this;
    }

    public function setRoute($routeName) {
        $this->clearConfig();
        $this->route = $routeName;
        return $this;
    }
    
    public function getAuto() {
        return $this->auto;
    }

    public function getRoute() {
        return $this->route;
    }
    
    //the field in entity that will be set to true on delete
    //without deleting row
    public function setField($field){
        $this->field = $field;
        return $this;
    }
    
    public function getField(){
        return $this->field;
    }
    
    public function getFieldMethod(){
        return 'set'.ucfirst($this->field);
    }

    private function clearConfig($fieldsToClear = true) {
        if($fieldsToClear === true){
            $this->route = null;
            $this->auto = null;
        }else{
            foreach($fieldsToClear as $field){
                $this->{$field} = null;
            }
        }
    }
}
<?php

namespace PO\AdminBundle\AdminCore;

class CustomAction{

    private $label;
    private $route;
    private $accessRoles = array();

    public function __construct($label, $name, $params) {
        $this->label = $label;
        $this->route = compact('name', 'params');
    }
    
    public function getRoute(){
        return $this->route;
    }
    
    public function getLabel(){
        return $this->label;
    }
    
    public function setAccess(array $roles){
        $this->accessRoles = $roles;
        return $this;
    }
    
    public function getAccessRoles(){
        return $this->accessRoles;
    }
    
}
<?php

namespace PO\AdminBundle\AdminCore\Dashboard;


abstract class Scaffold {
    
    const BOX_SMALL = 1;
    const BOX_MEDIUM = 2;
    const BOX_LARGE = 3;
    const ROW = 4;
    
    private $type;
    private $title;
    
    public function setTitle($title){
        $this->title = $title;
    }
    
    public function setType($type){
        $dashTypes = $this->getClassConstants();
        if(!array_search($type, $dashTypes))
              throw new \Exception("type: $type not recognized while initializing Dashboard");  
        
        $this->type = $type;
    }
    
    public function getType(){
        return $this->type;
    }
    
    public function getTitle(){
        return $this->title;
    }
    
    public function getClassConstants() {
        $reflect = new \ReflectionClass(get_class($this));
        return $reflect->getConstants();
    }
    
    public function getTypeCss(){
        switch($this->type){
            case 1:
                return 'box small';
                break;
            case 2:
                return 'box medium';
                break;
            case 3:
                return 'box large';
                break;
            case 4:
                return 'row';
                break;
        }
    }
    
    abstract public function getClassType();
    
}

?>

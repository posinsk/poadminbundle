<?php

namespace PO\AdminBundle\AdminCore\Dashboard;

final class Controller extends Scaffold {
    
    private $controller;
    private $args;
    
    public function __construct($title, $type) {
        $this->setType($type);
        $this->setTitle($title);
    }
    
    public function setController($controller, array $args = array()) {
        $this->controller = $controller;
        $this->args = $args;
    }
    
    public function getControllerName(){
        return $this->controller;
    }
    
    public function getArgs(){
        return $this->args;
    }
        
    public function getClassType(){
        return 'controller';
    }
}

?>

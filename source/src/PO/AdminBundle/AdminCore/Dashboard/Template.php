<?php

namespace PO\AdminBundle\AdminCore\Dashboard;

final class Template extends Scaffold {
    
    private $templateName;
    private $args;
    
    public function __construct($title, $type) {
        $this->setType($type);
        $this->setTitle($title);
    }
    
    public function setTemplate($template, array $args = array()){
        $this->templateName = $template;
        $this->args = $args;
    }
    
    public function getTemplateName(){
        return $this->templateName;
    }
    
    public function getArgs(){
        return $this->args;
    }
    
    public function getClassType(){
        return 'template';
    }
}

?>

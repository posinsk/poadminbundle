<?php
namespace PO\AdminBundle\AdminCore;

use Doctrine\ORM\EntityManager;

abstract class PositionCore{
    
    private $em;
    private $customActions;
    
    final public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    abstract public function listing();
    
    final public function getEntityManager(){
        return $this->em;
    }
    
    final public function addCustomAction(CustomAction $action){
        $this->customActions[] = $action;
    }
    
    final public function getCustomActions(){
        return $this->customActions;
    }
    
    final public function clearCustomActions(){
        $this->customActions = null;
    }

    /* If method return false, the sub-label will not be displayed */
    public function getSubLabel(){
        return false;
    }
    
}
<?php

namespace PO\AdminBundle\AdminCore;

class AdminList{
    
    private $query;
    
    private $fields = array();
    private $filters = array();
    private $orderBy = array();
    private $searchBy = array();
    private $minSearchByPhraseLenght = 3;
    
    private $filtersWithCounter = true;
    
    private $paginationDisabled = false;
    private $paginationPerPage;
    private $paginationPageRange;
    
    public function __construct($query) {
        $this->setQuery($query);
    }
    
    public function disablePagination(){
        $this->paginationDisabled =true;
    }
    
    public function isPaginationDisabled(){
        return $this->paginationDisabled;
    }

    private function setQuery(\Doctrine\ORM\QueryBuilder $query){
        $this->query = $query;
    }
    
    private function getListEntity(){
        $entities = $this->getQuery()->getRootEntities();
        return $entities[0];
    }
    
    public function getQuery(){
        return $this->query;
    }
    
    public function addSearchBy($label, $field){
        if(property_exists($this->getListEntity(), $field))
            $this->searchBy[] = compact('label', 'field');
        else
            throw new \Exception("Property '$field' does not exists in class: 
                                    {$this->getListEntity()}");
    }
    
    public function setMinSearchByPhraseLength($length){
        $this->minSearchByPhraseLenght = (int)$length;
    }
    
    public function getMinSearchByPhraseLength(){
        return $this->minSearchByPhraseLenght;
    }
    
    public function addField($label, $field, $args = array()){
        $this->fields[] = compact('label', 'field', 'args');
    }
    
    public function addFilter($label, $condition){
        $this->filters[] = compact('label', 'condition');
    }
    
    public function addOrderBy($label, $expression){
        $this->orderBy[] = compact('label', 'expression');
    }

    /**
     * Whether to show counter at filter label.
     * Every one filter means additional doctrine query!
     * Unless caching is enabled
     * @param boolean true/false
     */
    public function setFiltersWithCounter($boolean){
        $this->filtersWithCounter = (boolean)$boolean;
    }
    
    public function getFiltersWithCounter(){
        return $this->filtersWithCounter;
    }
    
    public function getFilters(){
        return $this->filters;
    }
    
    public function getFields(){
        return $this->fields;
    }
    
    public function getOrderBy(){
        return $this->orderBy;
    }
    
    public function getSearchBy(){
        return $this->searchBy;
    }
    
    public function setPaginationPageRange($pr){
        $this->paginationPageRange = (int)$pr;
    }
    
    public function setPaginationPerPage($pp){
        $this->paginationPerPage = (int)$pp;
    }
    
    public function getPaginationPerPage(){
        return $this->paginationPerPage;
    }
    
    public function getPaginationPageRange(){
        return $this->paginationPageRange;
    }
    
}

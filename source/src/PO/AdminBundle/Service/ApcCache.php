<?php

namespace PO\AdminBundle\Service;

/**
 * Description of Cache
 *
 * @author piotr
 */
class ApcCache
{
    public $apcLoaded = true;
    public $namespace;
    
    public function __construct(){
        if(!extension_loaded('apc')){
            $this->apcLoaded = false;
            return false;
        }
        
        $this->namespace = 'PO/AdminBundle:';
        
    }
    
    public function get($key){
        if(!$this->apcLoaded) return false;
        return apc_fetch($this->namespace.$key);
    }
    
    public function set($key, $value, $ttl = 0){
        if(!$this->apcLoaded) return false;
        apc_store($this->namespace.$key, $value, $ttl);
    }
    
    public function __call($method, $arguments){
        if(!$this->apcLoaded) return false;
    }
    
    
}

?>

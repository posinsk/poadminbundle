<?php

namespace PO\AdminBundle\Service\CmfLogger;

use Symfony\Component\Security\Core\SecurityContext;

class Subject
{

    private $identifier;
    private $name;
    private $username;
    private $userIdentifier;

    public function __construct(SecurityContext $user)
    {
        if ($user->getToken()) {
            $this->username = $user->getToken()->getUsername();

            $userId = method_exists($user->getToken()->getUser(), 'getId') ? true : false;
            $this->userIdentifier = $userId ? $user->getToken()->getUser()->getId() : null;
        }
    }

    public function setId($id)
    {
        $this->identifier = $id;
        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getId()
    {
        return $this->identifier;
    }

    public function getUsername()
    {
        return $this->userIdentifier ? "($this->identifier) " . $this->username : $this->username;
    }

    public function getUserId()
    {
        return $this->userIdentifier;
    }

}

?>

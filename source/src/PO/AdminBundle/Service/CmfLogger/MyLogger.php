<?php

namespace PO\AdminBundle\Service\CmfLogger;

use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Formatter\JsonFormatter;
use Monolog\Formatter\LineFormatter;

use PO\AdminBundle\Service\CmfLogger\Subject;

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Security\Core\SecurityContext;

class MyLogger
{
    private $instance;
    private $subject;
    private $rootDir;
    private $enabled;
    
    public function __construct(Kernel $kernel, SecurityContext $user){
        $this->rootDir = $kernel->getRootDir();
        
        $poAdminConfig = $kernel->getContainer()->getParameter('admin');
        $this->enabled = array_key_exists('logging', $poAdminConfig) ? $poAdminConfig['logging'] : false;

        $this->instance = new Logger('po_admin');
        if($this->enabled){
            $streamHandler = new RotatingFileHandler($this->rootDir.'/logs/poadminlog.log', 100);
            $streamHandlerAfterFormatting = $this->setFormatter($streamHandler);
            $this->instance->pushHandler($streamHandlerAfterFormatting);
        }
        
        $this->subject = new Subject($user);
        
        return $this->instance;
    }
    
    private function setFormatter($streamHandler){
        //psuedo json format, see ::parseLog() method to see how logs are parsed later
        $output = '{"date":"%datetime%", "level":"%level_name%", "message":"%message%"},'."\n";
        $formatter = new LineFormatter($output);
        
        $streamHandler->setFormatter($formatter);
        
        return $streamHandler;
    }
    
    public function log($message, $level = Logger::INFO){
        $this->instance->log($level, addslashes($message));
    }
    
    public static function parseLog($path) {
        $string = file_get_contents($path);

        //remove last ',' sign and add parenthesis to make parsable JSON
        $content = '[' . substr($string, 0, -2) . ']';
        
        //do the array reverse so the most recent logs are on top
        return array_reverse(json_decode($content));
    }
    
    public function setSubject($name, $id = null){
        $this->subject->setId($id);
        $this->subject->setName($name);
        return $this;
    }
    
    public function logCreate(){
        $s = $this->subject;
        $this->log('User "'.$s->getUsername().'" created "'.$s->getName().'", id: '.$s->getId());
    }
    
    public function logUpdate(){
        $s = $this->subject;
        $this->log('User "'.$s->getUsername().'" updated "'.$s->getName().'", id: '.$s->getId());
    }
    
    public function logRemove(){
        $s = $this->subject;
        $this->log('User "'.$s->getUsername().'" removed "'.$s->getName().'", id: '.$s->getId());
    }
    
    public function logFileUpload(){
        $s = $this->subject;
        $this->log('User "'.$s->getUsername().'" uploaded a file "'.$s->getName().'"');
    }
    
    public function logFileRemove(){
        $s = $this->subject;
        $this->log('User "'.$s->getUsername().'" removed a file "'.$s->getName().'"');
    }
    
    public function logDirCreate(){
        $s = $this->subject;
        $this->log('User "'.$s->getUsername().'" created a dir "'.$s->getName().'"');
    }
    
    public function logDirRemove(){
        $s = $this->subject;
        $this->log('User "'.$s->getUsername().'" removed a dir "'.$s->getName().'" with sub-directories and files');
    }
    
    public function logFilenameChange(){
        $s = $this->subject;
        $this->log('User "'.$s->getUsername().'" changed filename "'.$s->getName().'"');
    }
    
    public function logUserLogin(){
        $s = $this->subject;
        $this->log('User "'.$s->getUsername().'" logged in');
    }
    
    public function logCustomAction($from){
        $s = $this->subject;
        $this->log('User "'.$s->getUsername().'" done custom action "'.$s->getName().'" from "'.$from.'"');
    }
}

?>

<?php

namespace PO\AdminBundle\Service;

use Doctrine\ORM\QueryBuilder;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class Paginator{
    
    private $query;
    private $queryForCount;
    private $queryForId;
    private $page;
    private $idsForPage;
    private $totalCount;
    private $results;
    private $paginationData;
    private $rootIdentifier;
    
    private $em;
    
    private $numPerPage = 10;
    private $pageRange = 5;

    public function setConfig($perPage = 10, $pageRange = 3){
        if($perPage != null)
          $this->numPerPage = $perPage;
          $this->pageRange = $pageRange;
    }
    
    
    public function buildPage(QueryBuilder $query, $page, $orderByExpression = null){
        //pull out the root identifier
        $this->em = $query->getEntityManager();
        $rootEntites = $query->getRootEntities();
        $meta = $this->em->getClassMetadata($rootEntites[0]);
        $this->rootIdentifier = $meta->getSingleIdentifierFieldName(); //example (and mostly occured) result: 'id'
        
        $this->query = $query;
        $this->queryForCount = clone $query;
        $this->queryForId = clone $query; //while fetching the results select certain ids for this page
        $this->page = (int)$page;
        
        $this->count();
        $this->selectIdForPage($orderByExpression);//and put the in where in statement
        $this->getResults();
        
        return $this->results;
    }
    
    private function selectIdForPage($orderByExpression = null){
        $this->queryForId
                ->select("distinct {$this->query->getRootAlias()}.{$this->rootIdentifier}")
                ->setFirstResult(($this->page * $this->numPerPage) - $this->numPerPage)
                ->setMaxResults($this->numPerPage)
                ->addGroupBy("{$this->query->getRootAlias()}.id");
                        
                if ($orderByExpression) {
            $this->queryForId->addSelect($orderByExpression)
                    ->addGroupBy($orderByExpression);
               }
               
        $this->idsForPage = $this->queryForId
                ->getQuery()
                ->getResult();

        array_walk($this->idsForPage, function(&$val){
            $val = $val['id'];
        });
        
        return $this->idsForPage;
    }
    
    private function count(){
        $this->queryForCount->resetDQLPart('orderBy');

        $q = $this->queryForCount->select("count(distinct {$this->query->getRootAlias()})")
                ->getQuery()
                ->getSingleResult();
        $this->totalCount = $q[1];
        
        return $this->totalCount;
    }
    
    private function getResults() { 
        if($this->idsForPage){
            $this->results = $this->query
                    ->andWhere($this->query->getRootAlias() . ".{$this->rootIdentifier} IN (:whereIn)")
                    ->setParameter(':whereIn', $this->idsForPage)
                    ->getQuery()
                    ->getResult();
        }else
            $this->results = null;
        return $this->results;
    }
    
    public function getPaginationData()
    {
        $pageRange = $this->pageRange;       
        
        $pageCount = intval(ceil($this->totalCount / $this->numPerPage));
        $current = $this->page;

        if ($pageCount < $current) {
            $this->page = $current = $pageCount;
        }

        if ($pageRange > $pageCount) {
            $pageRange = $pageCount;
        }

        $delta = ceil($pageRange / 2);

        if ($current - $delta > $pageCount - $pageRange) {
            $pages = range($pageCount - $pageRange + 1, $pageCount);
        } else {
            if ($current - $delta < 0) {
                $delta = $current;
            }

            $offset = $current - $delta;
            $pages = range($offset + 1, $offset + $pageRange);
        }

        $viewData = array(
            'last' => $pageCount,
            'current' => $current,
            'numPerPage' => $this->numPerPage,
            'first' => 1,
            'pageCount' => $pageCount,
            'totalCount' => $this->totalCount,
            'pageRange' => $pageRange,
        );

        if ($current - 1 > 0) {
            $viewData['previous'] = $current - 1;
        }

        if ($current + 1 <= $pageCount) {
            $viewData['next'] = $current + 1;
        }

        $viewData['pagesInRange'] = $pages;
        $viewData['firstPageInRange'] = min($pages);
        $viewData['lastPageInRange']  = max($pages);

        if (count($this->results) != 0) {
            $viewData['currentItemCount'] = count($this->results);
            $viewData['firstItemNumber'] = (($current - 1) * $this->numPerPage) + 1;
            $viewData['lastItemNumber'] = $viewData['firstItemNumber'] + $viewData['currentItemCount'] - 1;
        }

        $this->paginationData = $viewData;
        return $this->paginationData;
    }
    
}

?>

<?php

namespace PO\AdminBundle\Service;

class UserAccessChecker {
    
    private $user;
    
    public function __construct(\Symfony\Component\Security\Core\SecurityContext $user) {
        $this->user = $user->getToken()->getUser();
    }
    
    public function checkAccess(array $roles){
        $allowed = false;
        
        if(empty($roles)) return true;
        
        foreach($this->user->getRoles() as $userRole){
            if(in_array($userRole, $roles)){
                    $allowed = true;
                    break;
            }
        }
        
        return $allowed;
    }
    
}

?>

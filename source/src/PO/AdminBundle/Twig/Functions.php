<?php

namespace PO\AdminBundle\Twig;

use Symfony\Component\DependencyInjection\Container;

class Functions extends \Twig_Extension {

    private $container;

    public function __construct(Container $container) {
        $this->container = $container;
    }

    public function getFunctions() {
        return array(
            'propertyCall' => new \Twig_Function_Method($this, 'propertyCall'),
            'propertyLabel' => new \Twig_Function_Method($this, 'propertyLabel'),
            'php' => new \Twig_Function_Method($this, 'php'),
            'menuPositionLabel' => new \Twig_Function_Method($this, 'menuPositionLabel'),
            'formatFilesize' => new \Twig_Function_Method($this, 'formatFilesize'),
            'globals' => new \Twig_Function_Method($this, 'globals'),
            'actionsArea' => new \Twig_Function_Method($this, 'actionsArea'),
            'checkAccess' => new \Twig_Function_Method($this, 'checkAccess'),
        );
    }
    
    public function checkAccess($roles){
        $access = $this->container->get('po.access');
        return $access->checkAccess($roles) ? true : false;
    }
    
    public function actionsArea($config){
        if(!method_exists($config, 'update')
                && !method_exists($config, 'delete')
                && !method_exists($config, 'read')
                && !method_exists($config, 'customActions'))
                return false;
        
        return true;
    }


    public function globals($param) {
        $config = $this->container->getParameter('admin');

        if ($param)
            return array_key_exists($param, $config);

        return $config;
    }

    public function formatFilesize($size) {
        $size = round($size / 1024, 1);
        if ($size <= 1024)
            return $size . ' Kb';

        $size = round($size / 1024, 1);
        return $size . ' Mb';
    }

    public function propertyCall($entity, $field) {
        if (!is_object($entity))
            throw new \Exception('Argument must be an entity object!');

        $exploded = explode('.', $field['field']);
        $count = count($exploded);
        foreach ($exploded as $k => $one) {
            $method = 'get' . ucfirst($one);
            $args = array_key_exists('args', $field) && $count == $k + 1 ? $field['args'] : array();

            if (isset($val)) {
                if($val instanceof \Doctrine\ORM\PersistentCollection){
                    $valArr = array();
                    foreach($val as $oneFromCollection)
                        $valArr[] = call_user_func_array(array($oneFromCollection, $method), $args);
                    $val = $valArr;
                }else
                    $val = call_user_func_array(array($val, $method), $args);
            }else
                $val = call_user_func_array(array($entity, $method), $args);
            
            if($val == null) break;
        }

        if ($val instanceof \DateTime)
            return $val->format('Y-m-d H:i:s');
        return $val;
    }
    
    public function php($function, $arg) {
        if (is_array($arg))
            return call_user_func_array($function, $arg);
        else
            return call_user_func($function, $arg);
    }

    public function menuPositionLabel($position) {
        $config = $this->container->getParameter('admin');
        return $config['menu'][$position];
    }

    public function getName() {
        return 'functions';
    }

}
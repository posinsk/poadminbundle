var $lc, $rc, $jswysiwyg;

var t=setTimeout(function(){resizeTemplate();},1000);

$(document).ready(function(){
    $lc = $('.left-column'),
    $rc = $('.right-column');
    $jswysiwyg = $('.js-wysiwyg');
    
    $('.js-confirm').bind('click', function(e){
        var r=confirm("Are you sure?");
        if (r==false) e.preventDefault();
    });
    
    initializeCollection();
    resizeTemplate();        
            
    $(window).resize(function() {
        resizeTemplate();
    });
    
    $('.js-add-file').bind('click', function(e){
        e.preventDefault();
        $('.files-add-dir').hide();
        $('.files-add-file').slideDown();
    });
    
    $('.js-add-dir').bind('click', function(e){
        e.preventDefault();
        $('.files-add-file').hide();
        $('.files-add-dir').slideDown();
    });
    
    $('.js-toggle-panel').bind('click', function(e){
        e.preventDefault();
        togglePanel();
    });
    
    if($jswysiwyg.length > 1){
        $jswysiwyg.each(function(k,el){
            initWysiHtml($(this).attr('id'), $(this).attr('name'));
        });
    }else if($jswysiwyg.length == 1)
       initWysiHtml($jswysiwyg.attr('id'), $jswysiwyg.attr('name'));

    $('.js-ajax-delete').bind('click', function(e){
        e.preventDefault();
        var $t = $(this);
        $.get($t.attr('href'), function(){
            $t.parent().parent().fadeOut();
        })
    });
    
});


initWysiHtml = function(id, name){
    var editor = new wysihtml5.Editor(id, {
        name: name,
        toolbar:      "toolbar-"+id,
        parserRules: wysihtml5ParserRules
    });
};

hasStorage = function() {
      try {
        localStorage.setItem('mod', 'mod');
        localStorage.removeItem('mod');
        return true;
      } catch(e) {
        return false;
      }
};

togglePanel = function(){
    var $hider = $('.hider');
    if($lc.css('display') != 'none'){
        $lc.animate({'margin-left': '-260px'}, function(){
            $(this).hide();
        });
        $rc.animate({'width':'100%'}, 400);
        $hider.find('.gliph').text('*');
        if(hasStorage()) localStorage.setItem('panel', 'hidden');
    }else{
        $rc.animate({'width':$(document).width() - $lc.width()-2},398);
        $lc.show();
        $lc.animate({'margin-left': '0px'});
        $hider.find('.gliph').text('(');
        if(hasStorage()) localStorage.setItem('panel', 'visible');
    }
};

resizeTemplate = function(){

    if($lc.css('display') != 'none')
        var rightColWidth = $(document).width() - $lc.width()-2;
    else
        var rightColWidth = '100%';
    
    $rc.width(rightColWidth);
    
    $lc.height($(document).height());
};

initializeCollection = function(){
    var collectionHolder = $('ul.js-collection-list');
    var $addLink = $('<a href="#" class="add">Add</a>');
    var $newLinkLi = $('<li></li>').append($addLink);
    
    jQuery(document).ready(function() {
        collectionHolder.find('li').each(function() {
            addFormWidgetDeleteLink($(this));
        });
        collectionHolder.append($newLinkLi);
        collectionHolder.data('index', collectionHolder.find(':input').length);
        $addLink.on('click', function(e) {
            e.preventDefault();
            addFormWidget(collectionHolder, $newLinkLi);
        });
    });
};

addFormWidget = function(collectionHolder, $newLinkLi) {
    var prototype = collectionHolder.data('prototype');
    var index = collectionHolder.data('index');
    var newForm = prototype.replace(/__name__/g, index);
    collectionHolder.data('index', index + 1);
    var $newFormLi = $('<li></li>').append(newForm);
    $newLinkLi.before($newFormLi);
    addFormWidgetDeleteLink($newFormLi);
};

addFormWidgetDeleteLink = function($tagFormLi) {
    var $removeFormA = $('<a href="#">Delete</a>');
    $tagFormLi.append($removeFormA);

    $removeFormA.on('click', function(e) {
        e.preventDefault();
        $tagFormLi.remove();
    });
};
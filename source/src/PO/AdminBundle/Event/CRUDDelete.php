<?php

namespace PO\AdminBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class CRUDDelete extends Event
{
    private $entity;
    private $name;
    
    public function __construct($name, $entity){
        $this->entity = $entity;
        $this->name = $name;
    }
    
    public function getName(){
        return $this->name;
    }
    
    public function getEntity(){
        return $this->entity;
    }
}

?>

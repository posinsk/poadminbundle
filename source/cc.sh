#!/bin/bash

sudo php -d memory_limit=256M app/console cache:clear

sudo chmod -R 777 app/cache
sudo chmod -R 777 app/logs

sudo php app/console cache:warmup

sudo chmod -R 777 app/cache
